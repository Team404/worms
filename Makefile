TARGET = main.out
CLTARGET = client.out
SETARGET = server.out
CXX = g++
# XXX: Don't forget backslash at the end of any line except the last one
LIBS = \
       -lsfml-graphics \
       -lsfml-system \
       -lsfml-window \
       -lsfml-network \
       -LSFML-2.5.0/lib/ -ISFML-2.5.0/include/

HDRS = project/include/

TSRCS = \
		physics/main.cpp \
		project/src/quadtree.cpp \
		project/src/constants.cpp

SRCS =  \
        project/src/main.cpp \
		project/src/constants.cpp \
        project/src/quadtree.cpp \
		project/src/constants.cpp

CLSRCS = \
        client/main.cpp \
        project/src/quadtree.cpp \
		project/src/constants.cpp \
		project/src/Resolution.cpp \
		project/src/UnitStatus.cpp \
		project/src/ShellStatus.cpp \
		project/src/PacketHandler.cpp \
		project/src/ClientPacketHandler.cpp \
		project/src/ClientWeapon.cpp \
		project/src/Worm.cpp \
		project/src/ConstConfig.cpp \
		project/src/ListenerButton.cpp \
		project/src/ClientState.cpp \
		project/src/Game.cpp \
		project/src/Client.cpp

SESRCS = \
		project/src/constants.cpp \
        server/main.cpp \
        project/src/quadtree.cpp \
        project/src/server.cpp \
        project/src/UnitStatus.cpp \
        project/src/ShellStatus.cpp \
        project/src/GameState.cpp \
        project/src/PacketHandler.cpp \
        project/src/ServerPacketHandler.cpp \
        project/src/ServerWeapon.cpp \
        project/src/ServerWorm.cpp \
        project/src/PhysicComponent.cpp \
        project/src/ShellPhysicComponent.cpp \
		project/src/ConstConfig.cpp

.PHONY: all clean client server

test: $(TSRCS)
	$(CXX) -Wall -Wextra -Werror $(addprefix -I,$(HDRS)) -o test.out $(CFLAGS) $(TSRCS) $(LIBS) -std=c++17

all: $(SRCS)
	$(CXX) -Wall -Wextra -Werror $(addprefix -I,$(HDRS)) -o $(TARGET) $(CFLAGS) $(SRCS) $(LIBS) -std=c++17

server: $(SESRC)
	$(CXX) -Wall -Wextra -Werror $(addprefix -I,$(HDRS)) -o $(SETARGET) $(CFLAGS) $(SESRCS) $(LIBS) -std=c++17

client: $(CLSRC)
	$(CXX) -Wall -Wextra -Werror -o $(CLTARGET) $(CFLAGS) $(CLSRCS) $(LIBS) -std=c++17 $(LIBS) $(addprefix -I,$(HDRS))

clean:
	rm -rf $(TARGET) $(CLTARGET) $(SETARGET)

