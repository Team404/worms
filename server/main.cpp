#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Server.hpp"

void play(Server& server) {
	server.start();
	sf::Clock clock;
	sf::Time timePerFrame = sf::seconds(1.0f / 60.0f);
	sf::Time fixed = sf::Time::Zero;
	while(!server.getEnd()) {
		sf::Time sftime = clock.getElapsedTime();
		clock.restart();
		fixed = fixed + sftime;
		while (sftime > timePerFrame) {
			sftime -= timePerFrame;
			server.update(timePerFrame.asMicroseconds()/800.0f);
		}
		server.update(sftime.asMicroseconds()/800.0f);
		server.recieve();
		if (fixed.asSeconds() > 1.0f / 30) {
			fixed = sf::Time::Zero;
			server.pushWormsStatus();
			server.pushShellStatus();
			server.updateWin();
			// std::cout << "Time is " << fixed.asMilliseconds() << std::endl;
			// std::cout << "Send packet" << std::endl;
			server.sendOutput();
			server.clear();
		}
	}
}

int main(int argc, char** argv) {
	int clientCount = 2;
	if (argc == 2) {
		clientCount = atoi(argv[1]);
	}
	
	Server server(clientCount, 1024);
	

	while(1) {
		play(server);
		server.restartRes();
	}
}
