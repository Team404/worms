#ifndef PROJECT_INCLUDE_CLIENT_WEAPON_HPP_
#define PROJECT_INCLUDE_CLIENT_WEAPON_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <memory>

#include "ClientShell.hpp"

class ClientWeapon {
 public:
 	ClientWeapon(int x, int y) {
		// TODO: Изменить текстуру. Текущая парсилка костыльна.
		sf::Image img;
		img.loadFromFile("image/weapon3.png");
		img.flipHorizontally();
		texture.loadFromImage(img);
 		sprite.setTexture(texture);
		sprite.setOrigin(sprite.getTextureRect().width / 2, (sprite.getTextureRect().height / 2) + 2);
		//
 		sprite.setPosition(x, y);
	}

	void update(int x ,int y, float angle);

	sf::Sprite& getSprite();

 private:
 	sf::Texture texture;
 	sf::Sprite sprite;
};

#endif  // PROJECT_INCLUDE_CLIENT_WEAPON_HPP_
