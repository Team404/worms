#ifndef WORMS_PROJECT_INCLUDE_CLIENT_HPP_
#define WORMS_PROJECT_INCLUDE_CLIENT_HPP_

#include <SFML/Network.hpp>
#include <iostream>
#include <memory>

#include "ClientPacketHandler.hpp"
#include "ConstConfig.hpp"

class Client {
	public:
		Client();
		
		bool start(sf::IpAddress ip, unsigned port, sf::RenderWindow& window, sf::Sprite& background,
				   unsigned qtSize = 1024, unsigned wormNumber = 1) {
   			serverIp = ip; 
			serverPort = port;
			if (socket.connect(serverIp, serverPort) != sf::Socket::Done) {
				return false;
			}   
			socket.setBlocking(false);
			state = std::make_unique<ClientState>(qtSize, wormNumber);
			ph = std::make_unique<ClientPacketHandler>(*state);

			if (!receiving(window, background, waitQT, Config::getInstance()->waitingTime)) {
				socket.disconnect();
				return false;
			}   

			if (!receiving(window, background, waitPL, Config::getInstance()->waitingTime)) {
				socket.disconnect();
				return false;
			}   

			if (!receiving(window, background, waitST, Config::getInstance()->waitingTime)) {
				socket.disconnect();
				return false;
			}   
			return true;
		}

		bool receiving(sf::RenderWindow& window, sf::Sprite& background, sf::Text& msg, float limit) {
			sf::Clock clock;
			float time = 0;
			sf::Time timer;
			float frame = 0;
			sf::String text = msg.getString();
			sf::Event event;
			while (!receivePacket() && timer <= sf::seconds(limit)) {
				while (window.pollEvent(event));
				window.clear();
				sf::Time sftime = clock.getElapsedTime();
				time = sftime.asMicroseconds();
				frame += time * 0.000005;
				if (frame > 4) {
					frame -= 4;
				}
				msg.setString(text);
				for (size_t i = 0; i < size_t(frame); ++i) {
					msg.setString(msg.getString() + sf::String("."));
				}
				timer += sf::microseconds(time);
				// std::cout << "TEST TIMER: " << timer.asSeconds() << std::endl;
				clock.restart();
				connected.setPosition(window.getSize().x / 2 - connected.getGlobalBounds().width / 2,
				                      window.getSize().y / 2 - (connected.getGlobalBounds().height / 2 - 10));
				msg.setPosition(window.getSize().x / 2 -  msg.getGlobalBounds().width / 2,
				                window.getSize().y / 2 - (msg.getGlobalBounds().height / 2 + 35));
				window.draw(background);
				window.draw(connected);
				window.draw(msg);
				// std::cout << "TEST1" << std::endl;
				window.display();
			}
			if (timer > sf::seconds(limit)) {
				window.clear();
				failed.setPosition(window.getSize().x / 2 - failed.getGlobalBounds().width / 2,
				                   window.getSize().y / 2 - (failed.getGlobalBounds().height / 2 - 10));
				window.draw(background);
				window.draw(failed);
				window.display();
				sf::sleep(sf::seconds(2));
				return false;
			}
			return true;
		}
	
		void update(float time) {
			receivePacket();
			state->update(time/800);
   		}
		bool receivePacket();
		void sendPacket(sf::Packet& packet) {
			sf::Socket::Status status = socket.send(packet);
			if (status == sf::Socket::Partial) {
				// std::cout << "Partial" << std::endl;
		   	}
		}
		void handleEvent(sf::Event event, sf::RenderWindow& win);
		void clearPackets() {
			inputPackets.clear();
		}

		void sendState() {
			sf::Packet packet;
	 		packet << Constants::WormStatus_ID << state->getWormNumber() << state->getPlayerState();
	 		sendPacket(packet);
		}

		ClientState& getState() {
			return *state;
		}

		~Client() {
			socket.disconnect();
		}

 	private:
 		sf::IpAddress serverIp;
 		sf::TcpSocket socket;
 		unsigned serverPort;
 		std::vector<std::unique_ptr<sf::Packet>> inputPackets;
 		std::unique_ptr<ClientPacketHandler> ph;
 		std::unique_ptr<ClientState> state;
		sf::Font font;
		sf::Text connected;
		sf::Text failed;
		sf::Text waitQT;
		sf::Text waitPL;
		sf::Text waitST;
};

#endif  // WORMS_PROJECT_INCLUDE_CLIENT_HPP_
