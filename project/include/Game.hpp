#ifndef WORMS_PROJECT_INCLUDE_GAME_HPP_
#define WORMS_PROJECT_INCLUDE_GAME_HPP_

#include <SFML/Network.hpp>
#include <iostream>
#include <cstdint>
#include <cmath>
#include <memory>

#include "Unit.hpp"
#include "Animations.hpp"
#include "BitMap.hpp"
#include "constants.hpp"
#include "quadtree.hpp"
#include "renderer.hpp"
#include "ResolutionSets.hpp"
#include "ClientPacketHandler.hpp"
#include "Button.hpp"
#include "ListenerButton.hpp"
#include "TextBar.hpp"
#include "Parser.hpp"
#include "Client.hpp"


double mouseAngle(const float dx, const float dy);

void startScreen(sf::RenderWindow& window);

class Game {
 public:
	struct ButtonManager {
		ButtonManager(sf::View& camera, sf::Font& font, ButtonFactory& factory);
	
		void update() {
			ButtonEvent butEvent = isPause ? listenerPause.getEvent() : listenerGame.getEvent();
			if (butEvent.status == Button::PRESSED) {
				handleButton(butEvent.type);
			}
			return;
		}
		void handleButton(Button::Type typeButton);
		
		bool isPause;
		bool isQuit;
		sf::RectangleShape exitMenu;
		sf::Text loseMsg;
		sf::Text winMsg;
		ListenerButton listenerGame;
		ListenerButton listenerPause;
		std::vector<std::shared_ptr <Button>> gameButtons;
		std::vector<std::shared_ptr <Button>> pauseButtons;
	};
	
	Game();

	void handleEvent(sf::Event event, ListenerButton& listener);

	bool update();

	void play();
 private:
	TextBox tb;
	TextBox newtb;
 	ButtonFactory factory;
	std::unique_ptr<Client> cl;
	sf::RenderWindow window;
	std::vector<std::shared_ptr <Button>> buttons;
	ListenerButton listenButton;
	sf::Texture fon;
	sf::Sprite fonImage;
	std::string IpPort;
	sf::Texture input;
	sf::Sprite inputImage;
	sf::Text text;
	sf::Font font;
	sf::Text errmsg;
	FocusController fc;
	ShadowAnimation shadow;
};

#endif  // WORMS_PROJECT_INCLUDE_GAME_HPP_
