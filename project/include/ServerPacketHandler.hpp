#ifndef PROJECT_INCLUDE_SERVER_PACKET_HANDLER_HPP_
#define PROJECT_INCLUDE_SERVER_PACKET_HANDLER_HPP_

#include "PacketHandler.hpp"
#include "GameState.hpp"

class ServerPacketHandler : public PacketHandler {
 public:
 	ServerPacketHandler(GameState& gamestate)
 	: state(gamestate) {
 	}

	void handleStatus(sf::Packet& packet);

	void handleExplosion(sf::Packet& packet);

	void handleQT(sf::Packet& packet);

	void handleNumber(sf::Packet& packet);

	void handleShells(sf::Packet& packet);

	void handleWin(sf::Packet& packet);

 private:
 	GameState& state;
};

#endif  // PROJECT_INCLUDE_SERVER_PACKET_HANDLER_HPP_