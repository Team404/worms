#ifndef WORMS_PROJECT_INCLUDE_ANIMATIONS_HPP_
#define WORMS_PROJECT_INCLUDE_ANIMATIONS_HPP_

#include <SFML/Graphics.hpp>
#include "ConstConfig.hpp"

class Animation {  // Класс, осуществляющий анимацию объекта
    private:
        float CurrentFrame;  // Время текущего кадра
        float MoveFrame;     // Частота обновления кадра (прибавляется к CurrentFrame)
        sf::Sprite sprite;
        const Frame frames;  // Лист тайлсетов для объекта
    public:
        Animation(/*sf::Sprite& sprite,*/float move_time, const Frame& list)
            :/*my_sprite(sprite),*/MoveFrame(move_time), frames(list) {
            CurrentFrame = 0;
            sprite.setTexture(frames.texture);
        }
        sf::Sprite& showFrame(/*sf::Sprite& sprite, */float time) {  // Обновление кадра
            CurrentFrame += MoveFrame * time;
            if (CurrentFrame > frames.num_frames) {   // Если все кадры показаны, начинать сначала
                CurrentFrame -= frames.num_frames;
            }
            sprite.setTextureRect(sf::IntRect(frames.x/* + int(CurrentFrame)*abs(frames.dx)*/,
                                              frames.y + (int(CurrentFrame) * abs(frames.dy))
                                                       + int(CurrentFrame),
                                              frames.dx,
                                              frames.dy));  // Обновление текстуры спрайта в
                                                            // соответствии с листом кадров
            return sprite;
        }
};

class ShadowAnimation {
	private:
		float MoveFrame;
		sf::RectangleShape shadow;
		sf::Color clr;
		float alpha;
	public:
		ShadowAnimation(float frame, sf::Vector2f size, sf::Color c, float a = 80)
			: MoveFrame(frame), shadow(size), clr(c), alpha(a) {
			clr.a = alpha;
			shadow.setFillColor(clr);
		}
		sf::RectangleShape& showFrame() {
			return shadow;
		}
		sf::RectangleShape& showUpFrame(float time) {
			if ((alpha += MoveFrame * time) > 255) {
				alpha = 255;
			}
			clr.a = alpha;
			shadow.setFillColor(clr);
			return shadow;
		}
		sf::RectangleShape& showDownFrame(float time) {
			if ((alpha -= MoveFrame * time) < 0) {
				alpha = 0;
			}
			clr.a = alpha;
			shadow.setFillColor(clr);
			return shadow;
		}
		void setAlpha(float a) {
			alpha = a;
			clr.a = alpha;
			shadow.setFillColor(clr);
			return;
		}
		bool isFill() {
			return alpha >= 255.0;
		}
		bool isAlpha() {
			return alpha <= 0.0;
		}
};

#endif  // WORMS_PROJECT_INCLUDE_ANIMATIONS_HPP_
