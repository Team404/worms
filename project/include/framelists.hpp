#ifndef PROJECT_INCLUDE_FRAMELISTS_HPP_
#define PROJECT_INCLUDE_FRAMELISTS_HPP_

#include <SFML/Graphics.hpp>


struct Frame {
	int x;
	int y;
	int dx;
	int dy;
	size_t num_frames;
	sf::Texture texture;
	Frame(sf::String imgFile, int x, int y, int dx, int dy, size_t n)
		: x(x), y(y), dx(dx), dy(dy), num_frames(n) {
		texture.loadFromFile("image/" + imgFile);
	}
	Frame() {
	}
};

#endif  // PROJECT_INCLUDE_FRAMELISTS_HPP_
