#ifndef PROJECT_INCLUDE_PACKET_HPP_
#define PROJECT_INCLUDE_PACKET_HPP_

#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include "quadtree.hpp"
#include "constants.hpp"
#include "BitMap.hpp"

class work_packet {
 public:
	work_packet(sf::Packet p) : packet(p) {}
	receive(QuadTree* qt);
	send(sf::Event& event, ServerSimulation& server, QuadTree* qt);
 private:
	bool pressed;
	sf::Packet packet;
};

