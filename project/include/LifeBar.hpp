#ifndef PROJECT_INCLUDE_LIFEBAR_HPP_
#define PROJECT_INCLUDE_LIFEBAR_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class LifeBar {
 public:
	LifeBar(int size_x)
	: max(100), size_x(size_x) {
		image.loadFromFile("image/bar1.jpg");
		t.loadFromImage(image);
		s.setTexture(t);
		s.setTextureRect(sf::IntRect(0, 0, size_x, 7));
		bar.setFillColor(sf::Color(0, 0, 0)); //черный прямоугольник накладывается сверху и появляется эффект отсутствия здоровья
	}
 
	void update(int k, sf::Rect<sf::Int16> rect) { // k-текущее здоровье
		if (k>=0) {
			s.setTextureRect(sf::IntRect(0, 0, 20 + k, 7));
		} else {
			s.setTextureRect(sf::IntRect(0,0,20,7));
		}
		s.setPosition(rect.left + rect.width / 2 - size_x / 2, rect.top - 10);
		bar.setPosition(rect.left + 4 + rect.width / 2 - size_x / 2, rect.top - 14);
	}

	const sf::Sprite& getSprite() const {
		return s;
	}

	const sf::RectangleShape getBar() const {
		return bar;
	}
 private:
	sf::Image image;
	sf::Texture t;
	sf::Sprite s;
	int max;
	int size_x;
	sf::RectangleShape bar;
};

#endif  // PROJECT_INCLUDE_LIFEBAR_HPP_
