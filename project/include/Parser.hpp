#ifndef PROJECT_INCLUDE_PARSER_HPP_
#define PROJECT_INCLUDE_PARSER_HPP_

#include <iostream>
#include <string>

class Parser {
 public:
	 Parser(sf::Text src) {
		 std::string newsrc = src.getString();
		 size_t n = newsrc.find(':');
		 ip = (n != std::string::npos) ? newsrc.substr(0, n) : "";
		 port = (n != std::string::npos) ? newsrc.substr(n + 1, newsrc.length()) : "";
	 }

	 sf::IpAddress getIp() {
		 return static_cast<sf::IpAddress>(ip);
	 }

	 unsigned getPort() {
		std::cout << port << std::endl;
		 return std::stoi(port, 0, 10);
	 }

 private:
	 std::string ip;
	 std::string port;
};

#endif  // PROJECT_INCLUDE_PARSER_HPP_

