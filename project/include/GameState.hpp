#ifndef PROJECT_INCLUDE_GAMESTATE_HPP_
#define PROJECT_INCLUDE_GAMESTATE_HPP_

#include <SFML/Graphics.hpp>
#include <memory>
#include <list>

#include "ServerWorm.hpp"
#include "UnitStatus.hpp"
#include "BitMap.hpp"
#include "quadtree.hpp"
#include "ConstConfig.hpp"

struct Explosion {
	Explosion(int x, int y, int r) 
	: x(x), y(y), r(r) {
	}
	
	int x;
	int y;
	int r;
};

class GameState {
 public:
	GameState(unsigned qtSize = 1024, unsigned playerCount = 2)
	: qt(qtSize), countAlive(playerCount) {
		sf::Image img;
		if (!img.loadFromFile("image/vyetnam.png")) {
			std::cerr << "Error load img" << std::endl;
		}

		BitMap bm(img);

		for (size_t i = 0; i < qt.getData().size; ++i) {
			for (size_t j = 0; j < qt.getData().size; ++j) {
				qt.getNode(j, i)->setState(bm.get(j, i));
			}
		}

		ServerWormFactory factory(qt, 24, 26);

		for (unsigned i = 0; i < playerCount; ++i) {
			worms.push_back(std::move(static_unique_pointer_cast<ServerWorm>(std::move(factory.create(Config::getInstance()->respawnPoints[i])))));
		}
	}

	void update(float time);

	unsigned getCountAlive();

	unsigned getPlayerCount();

	void handleWormExplosion(ServerWorm& worm, Explosion& expl);

	void handleExplosions();

	std::vector<Explosion>& getExplosions();

	std::list<std::unique_ptr<ServerShell>>& getShells();

	void clearExplosions();

	void makeExplosion(unsigned x, unsigned y, unsigned r);

	void updateWormState(sf::Uint8 number, UnitStatus& state);

	QuadTree& getQT();

	std::vector<std::unique_ptr<ServerWorm>>& getWorms();

 private:
	QuadTree qt;
	std::vector<std::unique_ptr<ServerWorm>> worms;
	std::list<std::unique_ptr<ServerShell>> shells;
	std::vector<Explosion> explosions;
	unsigned countAlive;
};

#endif  // PROJECT_INCLUDE_GAMESTATE_HPP_
