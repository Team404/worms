#ifndef PROJECT_INCLUDE_CLIENT_PACKET_HANDLER_HPP_
#define PROJECT_INCLUDE_CLIENT_PACKET_HANDLER_HPP_

#include "PacketHandler.hpp"
#include "ClientState.hpp"

class ClientPacketHandler : public PacketHandler {
 public:
 	ClientPacketHandler(ClientState& state)
 	: state(state) {
 	}

	void handleExplosion(sf:: Packet& packet);

	void handleStatus(sf::Packet& packet);

	void handleQT(sf::Packet& packet);

	void handleNumber(sf::Packet& packet);

	void handleShells(sf::Packet& packet);

	void handleWin(sf::Packet& packet);

 private:
 	ShellClientFactory shellFactory;
	ClientState& state;
};

#endif  // PROJECT_INCLUDE_CLIENT_PACKET_HANDLER_HPP_