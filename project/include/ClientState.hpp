#ifndef PROJECT_INCLUDE_CLIENTSTATE_HPP_
#define PROJECT_INCLUDE_CLIENTSTATE_HPP_

#include <vector>
#include <list>
#include <memory>

#include "quadtree.hpp"
#include "Worm.hpp"

class ClientState {
 public:
	ClientState(unsigned qtSize, unsigned wormNumber)
	: qt(qtSize), wormNumber(wormNumber), win(0) {
		WormFactory factory;
	}

	void update(float time);

	UnitStatus& getPlayerState();

	QuadTree& getQT();

	std::vector<std::unique_ptr<Worm>>& getWorms();

	std::list<std::unique_ptr<ClientShell>>& getShells();

	void explosion(int x, int y, int r);

	void setWormNumber(int number);

	sf::Int8 getWormNumber();

	void updateWormState(std::vector<std::unique_ptr<UnitStatus>>& states);

	void setWin(int win);

	int getWin();
 private:
	QuadTree qt;
	WormFactory factory;
	std::vector<std::unique_ptr<Worm>> worms;
	std::list<std::unique_ptr<ClientShell>> shells;
	sf::Uint8 wormNumber;
	int win;
};

#endif  // PROJECT_INCLUDE_CLIENTSTATE_HPP_
