#ifndef PROJECT_INCLUDE_SERVERWORM_HPP_
#define PROJECT_INCLUDE_SERVERWORM_HPP_

#include "Unit.hpp"

class ServerWorm : public Unit {
 public:
 	ServerWorm(QuadTree& qt, int x = 0, int y = 0, int width = 24, int height = 26)
 	: Unit(x, y, width, height), phc(&getState(), qt) {
 		weapons.emplace_back(std::make_unique<ServerWeapon>(qt, x + (width / 2), y + (height / 2)));
 	}

 	void getImpulse(sf::Vector2f impulse);

	void decreaseHP(float diff);

	void update(float time);

	ServerWeapon& getCurrentWeapon();

	void updateShooting(float time);

	std::unique_ptr<ServerShell> shoot();
 private:
	PhysicComponent phc;
	std::vector<std::unique_ptr<ServerWeapon>> weapons;
};

class ServerWormFactory : public UnitFactory {
 public:
	ServerWormFactory(QuadTree& qt, int width, int height)
	: UnitFactory(width, height), qt(qt) {
	}

	std::unique_ptr<Unit> create(int x, int y) {
	 	return std::make_unique<ServerWorm>(qt, x, y, width, height);
	}

	std::unique_ptr<Unit> create(sf::Vector2i pos) {
	 	return std::make_unique<ServerWorm>(qt, pos.x, pos.y, width, height);
	}
 private:
	QuadTree& qt;
};

#endif  // PROJECT_INCLUDE_SERVERWORM_HPP_