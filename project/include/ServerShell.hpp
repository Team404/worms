#ifndef	WORMS_PROJECT_SERVER_SHELL_HPP_
#define WORMS_PROJECT_SERVER_SHELL_HPP_

#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"

#include "UnitStatus.hpp"
#include "ShellStatus.hpp"
#include "quadtree.hpp"
#include "ShellPhysicComponent.hpp"

class ServerShell {
 public:
	ServerShell(QuadTree& qt, int x, int y, sf::Vector2f startImpulse, int width = 10, int height = 10, sf::Int16 radius = 32)
	: state(x, y, width, height, radius), ph(qt, &state) {
		ph.getImpulse(startImpulse);
	}
	
	bool isAlive() {
	 	return !state.explose;
	}

	void update(float time) {
		ph.update(time);
	}

	ShellStatus& getState() {
	 	return state;
	}

	void pushCollision(sf::Rect<sf::Int16>& rect) {
		ph.pushCollision(rect);
	}

	void clearCollision() {
		ph.clearCollision();
	}
 private:
 	ShellStatus state;
	ShellPhysicComponent ph;
};

class ShellServerFactory {
 public:
 	ShellServerFactory(QuadTree& qt) 
 	: qt(qt) {
 	}

 	std::unique_ptr<ServerShell> create(sf::Vector2i pos, sf::Vector2f impulse) {
 		return std::make_unique<ServerShell>(qt, pos.x + int(33 * impulse.x) /* - 40*/, pos.y + int(35 * impulse.y)/* - 40*/, impulse);
 	}

 private:
 	QuadTree& qt;
};

#endif  // WORMS_PROJECT_SERVER_SHELL_HPP_


