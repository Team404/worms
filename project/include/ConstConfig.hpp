#ifndef WORMS_PROJECT_CONSTCONFIG_HPP_
#define WORMS_PROJECT_CONSTCONFIG_HPP_

#include <fstream>
#include <iostream>
#include <SFML/System.hpp>
#include <sstream>
#include "framelists.hpp"
	
	struct AnimationConsts {
		size_t StayLeft;
		size_t StayRight;
		size_t MoveLeft;
		size_t MoveRight;
		size_t JumpLeft;
		size_t JumpRight;
		size_t FallLeft;
		size_t FallRight;
		size_t Dead;
	};

	struct FrameInfo {
		Frame WormStayLeftTile;
		Frame WormStayRightTile;
		Frame WormLeftTile;
		Frame WormRightTile;
		Frame WormJumpLeftTile;
		Frame WormJumpRightTile;
		Frame WormFallLeftTile;
		Frame WormFallRightTile;
		Frame WormDeadTile;
	};

class Config {
 public:
	enum stringIP {
		StayLeft,
		StayRight,
		MoveLeft,
		MoveRight,
		JumpLeft,
		JumpRight,
		FallLeft,
		FallRight,
		Dead,
		Gravity_ID,
		Space_ID,
		FrictionCoefficient_ID,
		GameSpeed_ID,
		WormSpeed_ID,
		ShellMass_ID,
		WormMass_ID,
		LifeBarSize_ID,
		WormStayLeftTile_ID,
		WormStayRightTile_ID,
		WormLeftTile_ID,
		WormRightTile_ID,
		WormJumpLeftTile_ID,
		WormJumpRightTile_ID,
		WormFallLeftTile_ID,
		WormFallRightTile_ID,
		WormDeadTile_ID,
		weaponForce_ID,
		maxForce_ID,
		reloadTime_ID,
		respawnPoint1,
		respawnPoint2,
		respawnPoint3,
		respawnPoint4,
		waitingTime_ID,
		weaponDamage_ID,
	};

	float waitingTime;
	sf::Vector2i respawnPoints[4];
	float weaponForce;
	int maxForce;
	int reloadTime;
	int ShellMass;
	int WormMass;
	int LifeBarSize;
	sf::Vector2f Gravity;
	sf::Vector2f Space;
	float GameSpeed;
	float FrictionCoefficient;
	float WormSpeed;
	int weaponDamage;
	int hashit (std::string const& name);

	static Config* getInstance();
	FrameInfo getFrameInfo() const;
	AnimationConsts getAnime() const;	
	~Config();
	Config& operator =(Config const&) = delete;
	Config(Config const&) = delete;
	bool openConfigFile(const char* path);

 private:
	AnimationConsts anime;
	FrameInfo frinfo;
	Config() {}	
	static Config* myConfig;
};

#endif  // WORMS_PROJECT_CONSTCONFIG_HPP_

