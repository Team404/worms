#ifndef PROJECT_INCLUDE_TEXTBAR_HPP_
#define PROJECT_INCLUDE_TEXTBAR_HPP_

#include <SFML/Graphics.hpp>
#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <locale>

class InpText {
 public:
	InpText() {
		if (!font.loadFromFile("/usr/share/fonts/truetype/ttf-khmeros-core/KhmerOS.ttf")) {
		}
		input.setPosition(sf::Vector2f(200,200));
		input.setFont(font);
		input.setCharacterSize(25);
		input.setString("192.168.43.216");
	}
		
	InpText(std::string ipPort, sf::Vector2f pos) {
		if (!font.loadFromFile("/usr/share/fonts/truetype/ttf-khmeros-core/KhmerOS.ttf")) {
        //return 11;
		}
		input.setPosition (pos);
		input.setFont(font) ;
		input.setString(ipPort);
		input.setCharacterSize(17);
	}

	sf::Text getText() {
		return input;
	}
 private:
	sf::Text input;
	sf::Font font;
};

 
class FocusObject {
 public:
    virtual void setFocus() = 0;
    virtual void deleteFocus() = 0;
    virtual void event(const sf::Event&) = 0;
};

class TextBox : /*public sf::Drawable, */public FocusObject, public sf::Transformable {
 public:
    TextBox (sf::Text text) : m_text(text) {
		std::cout << std::endl;
        m_newText = m_text.getString();
        updateRect();
        m_box.setFillColor( sf::Color::Blue ) ;
    }

	TextBox() {
		InpText inp;
		m_text = inp.getText();
        m_newText = m_text.getString();
        updateRect();
        m_box.setFillColor(sf::Color::Blue);
	}

    virtual void draw(sf::RenderTarget& render, sf::RenderStates states) const {
        if (m_textChanged) {
            const_cast<TextBox*>(this)->setText(m_newText);
            m_textChanged = false;
        }
        render.draw(m_box, states);
        render.draw(m_text, states);
    }

    virtual void setFocus() {
        m_box.setFillColor(sf::Color::Red);
    }

    virtual void deleteFocus() {
        m_box.setFillColor(sf::Color::Blue);
    }

    virtual void event(const sf::Event& event) {
        if (event.type == sf::Event::TextEntered) {
            m_textChanged = true; 
            switch (event.text.unicode) {
				case 0x8://Backspace
					if (!m_newText.isEmpty()) {
						m_newText.erase(m_newText.getSize() - 1);
					}
					m_text.setString(m_newText);
					break ;
				default : {
					if (m_newText.getSize() < 22) {
						m_newText += static_cast<wchar_t>(event.text.unicode);
    					m_text.setString(m_newText);
                    }
				}
			}
        }
    }

    void setText(const sf::String& str) {
        m_text.setString(str);
        updateRect();
    }
	sf::Text getText() {
		return m_text;
	}

	std::string getString() {
		return m_newText;
	}

 private:
    void updateRect() {
		sf::Vector2f pos(600,600);
		sf::Vector2f size(100, 50);
        sf::FloatRect rect(pos,size);
        m_box.setPosition (rect.left - 5, rect.top - 5);
        m_box.setSize(sf::Vector2f(rect.width + 10, rect.height + 10));
    }
    mutable sf::RectangleShape m_box;
    sf::Text m_text;
    mutable sf::String m_newText;
    mutable bool m_textChanged;
	std::string result;
};
 
class FocusController {
 public:
    FocusController(FocusObject* obj = 0) : m_object(obj) {
        if (m_object != 0) {
            m_object->setFocus();
        }
    }

    void setFocusObject(FocusObject * new_obj) {
        if (m_object == new_obj) {
            return;
		}
        if (m_object != 0) {
            m_object->deleteFocus();
		}
        if (new_obj != 0) {
            new_obj->setFocus();
		}
        m_object = new_obj;
    }

    FocusObject* getFocusObject() {
        return m_object;
    }
 
private:
    FocusObject* m_object;
};

#endif  // PROJECT_INCLUDE_TEXTBAR_HPP_
