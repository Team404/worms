#ifndef PROJECT_INCLUDE_ANIMATIONS_CONST_H
#define PROJECT_INCLUDE_ANIMATIONS_CONST_H

#include <cstddef>


namespace AnimationConsts {
	const size_t StayLeft(0);
	const size_t StayRight(1);
	const size_t MoveLeft(2);
	const size_t MoveRight(3);
	const size_t JumpLeft(4);
	const size_t JumpRight(5);
	const size_t FallLeft(6);
	const size_t FallRight(7);
	const size_t Dead(8);
}

#endif  // PROJECT_INCLUDE_ANIMATIONS_CONST_H
