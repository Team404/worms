#ifndef PROJECT_INCLUDE_PACKETHANDLER_HPP_
#define PROJECT_INCLUDE_PACKETHANDLER_HPP_

#include <SFML/Network.hpp>
#include <memory>
#include <vector>

#include "quadtree.hpp"
#include "constants.hpp"
#include "UnitStatus.hpp"
#include "Unit.hpp"
//#include "ConstConfig.hpp"

class PacketHandler {
 public:

	void handlePacket(sf::Packet& packet);

	void handleVector(std::vector<std::unique_ptr<sf::Packet>>& packets) ;

	virtual void handleExplosion(sf::Packet& packet) = 0;
	
	virtual void handleNumber(sf::Packet& packet) = 0;
	
	virtual void handleQT(sf::Packet& packet) = 0;
	
	virtual void handleStatus(sf:: Packet& packet) = 0;
	
	virtual void handleShells(sf::Packet& packet) = 0;
	
	virtual void handleWin(sf::Packet& packet) = 0;

	virtual ~PacketHandler() = default;
};

#endif  // PROJECT_INCLUDE_PACKETHANDLER_HPP_
