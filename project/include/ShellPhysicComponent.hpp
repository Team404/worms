#ifndef PROJECT_INCLUDE_SHELL_PHYSICCOMPONENT_HPP_	
#define PROJECT_INCLUDE_SHELL_PHYSICCOMPONENT_HPP_

#include <SFML/System.hpp>
#include <cmath>
#include <iostream>

#include "quadtree.hpp"
#include "UnitStatus.hpp"
#include "ShellStatus.hpp"
#include "ConstConfig.hpp"

class ShellPhysicComponent {  // TODO: сделайте что-то с этим кодом, у него должен быть общий интерфейс с PhysicComponent
 public:
 	ShellPhysicComponent(QuadTree& qt, ShellStatus* state)
 	: mass(Config::getInstance()->ShellMass), qt(qt), state(state) {
 	}

 	void update(float time);

 	void getImpulse(sf::Vector2f umpulsVelocity);
 	
 	void falling(float time);

 	void handleVelocity(float time);

	bool checkVertical(int mode);

	bool checkHorizontal(int mode);

	int move(sf::Vector2i direction);

	void pushCollision(sf::Rect<sf::Int16>& rect);

	void clearCollision();

 private:
 	int mass;
 	sf::Vector2f velocity;
 	sf::Vector2f accumulatedMove;
 	QuadTree& qt;
 	ShellStatus* state;
 	std::vector<sf::Rect<sf::Int16>> collisionRects;
};

#endif  // PROJECT_INCLUDE_SHELL_PHYSICCOMPONENT_HPP_
