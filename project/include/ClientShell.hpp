#ifndef	WORMS_PROJECT_CLIENT_SHELL_HPP_
#define WORMS_PROJECT_CLIENT_SHELL_HPP_

#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"

#include "ShellStatus.hpp"
#include "ShellPhysicComponent.hpp"

class ClientShell {
 public:
	 ClientShell(sf::Texture& texture) {
	 	sprite.setTexture(texture);
		sprite.setOrigin(sprite.getTextureRect().width / 2, sprite.getTextureRect().height / 2);
	 }

	 bool isAlive() {
	 	return !state.explose;
	 }

	 void update() {
	 	if (isAlive()) {
	 		sprite.setPosition(state.rect.left, state.rect.top);
	 		sprite.setRotation(state.direction);
	 	}
	 }

	 ShellStatus& getState() {
	 	return state;
	 }

	 sf::Sprite& getSprite() {
	 	return sprite;
	 }

 private:
	 sf::Sprite sprite;
	 ShellStatus state;
};

class ShellClientFactory {
 public:
 	ShellClientFactory() {
 		texture.loadFromFile("image/shell.png");
 	}

 	std::unique_ptr<ClientShell> create() {
 		sf::Sprite sprite;
 		return std::make_unique<ClientShell>(texture);
 	}
 private:
 	sf::Texture texture;
};

#endif  // WORMS_PROJECT_CLIENT_SHELL_HPP_