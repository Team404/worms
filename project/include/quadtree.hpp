#ifndef QUADTREE_HPP_
#define QUADTREE_HPP_

#include <iostream>
#include <SFML/Network.hpp>

class RangeException {
 public:
 	RangeException(bool down)
 	: down(down) {
 	}

 	bool getDown() {
 		return down;
 	}
 private:
 	bool down;
};

struct Data {
	Data(unsigned x, unsigned y, unsigned size, short state = 0)
	: x(x), y(y), size(size), state(state) {
	}

	Data(unsigned size, short state = 0)
	: x(0), y(0), size(size), state(state) {
	}

	Data subData(int mode);
	int x;
	int y;
	unsigned size;
	short state;  // state = 0 - пустой, state = 1 - заполненный, state = -1 - частично пустой, частично заполненный
};

class QuadTree {
 public:
 	QuadTree() = default;

	QuadTree(unsigned size, QuadTree* parent = nullptr)
	: data(size), parent(parent) {
		for (int i = 0; i < 4; ++i) {
			childs[i] = nullptr;
		}
	}

	QuadTree(Data&& data, QuadTree* parent = nullptr)
	: data(data), parent(parent) {
		for (int i = 0; i < 4; ++i) {
			childs[i] = nullptr;
		}
	}
	QuadTree* getChild(int mode) {
		if (mode > 3 || mode < 0) {
			throw -2;  //TODO: добавить исключения
		}
		return childs[mode];
	}

	QuadTree* getNode(int x, int y);

	void setState(short st);

	short getState() const {
		return data.state;
	}

	Data getData() const {
		return data;
	}

	void explosion(unsigned x, unsigned y, int r);

	void pool();

	void separateNode();

	void separate();

	short getNodeState(int x, int y) const;

	friend sf::Packet& operator <<(sf::Packet& packet, const QuadTree& qt);
	friend sf::Packet& operator >>(sf::Packet& packet, QuadTree& qt);

	~QuadTree();

	void print();
 private:
	void updateState();

	bool checkEqualState();

	Data data;
	QuadTree* childs[4];
	QuadTree* parent;
};

#endif  // QUADTREE_HPP_