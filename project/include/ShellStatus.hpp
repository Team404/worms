#ifndef PROJECT_INCLUDE_SHELLSTATUS_HPP_
#define PROJECT_INCLUDE_SHELLSTATUS_HPP_

#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include "UnitStatus.hpp"

struct ShellStatus {
	ShellStatus(int x = 0, int y = 0, int width = 0, int height = 0, sf::Int16 radius = 32) 
	: rect(x, y, width, height), direction(0), radius(radius), explose(0) {
	}
	sf::Rect<sf::Int16	> rect;
	sf::Int16 direction;  // угол, 0 - вправо, 180 - vlevo
	sf::Int16 radius;
	sf::Int8 explose;
};

sf::Packet& operator <<(sf::Packet& packet, const ShellStatus& state);

sf::Packet& operator >>(sf::Packet& packet, ShellStatus& state);

#endif  // PROJECT_INCLUDE_SHELLSTATUS_HPP_