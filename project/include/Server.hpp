#ifndef PROJECT_INCLUDE_SERVER_HPP_
#define PROJECT_INCLUDE_SERVER_HPP_

#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <memory>

#include "ServerPacketHandler.hpp"
#include "GameState.hpp"

class Server {
 public:
 	Server(unsigned clientNum, unsigned qtSize)
 	: end(false), clientNum(clientNum), qtSize(qtSize), port(0), clientConnected(0) {
 	}

	void start();

	void update(float time);

	void updateWin();

	bool recieve();

	void pushWormsStatus();

	void pushShellStatus();

	void sendWormNumber();

	void sendOutput();

	void sendPacket(sf::Packet& packet);

	bool getEnd();

	void clear();

	void restartRes();

 private:
	sf::TcpListener listener;
	sf::SocketSelector selector;
	std::vector<std::unique_ptr<sf::TcpSocket>> clients;
	std::unique_ptr<GameState> gamestate;
	std::unique_ptr<ServerPacketHandler> ph;
	std::vector<std::unique_ptr<sf::Packet>> outputPackets;
	bool end;

	const unsigned clientNum;
	const unsigned qtSize;
	unsigned port;

	int clientConnected;
};

#endif  // PROJECT_INCLUDE_SERVER_HPP_