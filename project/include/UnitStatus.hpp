#ifndef PROJECT_INCLUDE_UNITSTATUS_HPP_
#define PROJECT_INCLUDE_UNITSTATUS_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <iostream>

enum OnGround : sf::Int8 {
	ON_GROUND = 0,
	READY_TO_JUMP,
	JUMP,
	FALLING,
	CLIMBING
};

enum Shooting : sf::Int8 {
	NOT_SHOOT = 0,
	SHOOTING,
	SHOOT,
};

struct UnitStatus {
	UnitStatus(int x = 0, int y = 0, int width = 0, int height = 0) 
	: rect(x, y, width, height), direction(0), onGround(ON_GROUND), moveRight(0),
	  moveLeft(0), shooting(NOT_SHOOT), currentWeapon(0), hp(100) {
	}
	sf::Rect<sf::Int16> rect;
	sf::Int16 direction;  // угол, 0 - вправо, 180 - vlevo
	OnGround onGround;
	sf::Int8 moveRight/*: 1*/;
	sf::Int8 moveLeft/*: 1*/;
	Shooting shooting;
	sf::Int8 currentWeapon;
	sf::Int8 hp;
};

std::ostream& operator <<(std::ostream& os, UnitStatus& us);

sf::Packet& operator <<(sf::Packet& packet, const sf::Rect<sf::Int16>& rect);

sf::Packet& operator <<(sf::Packet& packet, const UnitStatus& us);

sf::Packet& operator >>(sf::Packet& packet, sf::Rect<sf::Int16>& rect);

sf::Packet& operator >>(sf::Packet& packet, UnitStatus& us);


#endif  // PROJECT_INCLUDE_UNITSTATUS_HPP_
