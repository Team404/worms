#ifndef WORMS_PROJECT_INCLUDE_LISTENERBUTTON_HPP_
#define WORMS_PROJECT_INCLUDE_LISTENERBUTTON_HPP_

#include "Button.hpp"


struct ButtonEvent {
	ButtonEvent(Button::Type type = Button::DEFAULT, Button::Status status = Button::NOT_PRESSED)
	   	: type(type), status(status) {
	}
	Button::Type type;
	Button::Status status;
};
		


class ListenerButton {
 public:
	void add(std::shared_ptr<Button>& but);

	void updatePressed(sf::Vector2f mousePos);

	void updateWaiting(sf::Vector2f mousePos);

	void updateEvent(Button& but);

	ButtonEvent getEvent();
 private:
	ButtonEvent event;
	std::vector<std::shared_ptr<Button>> buttons;
};

#endif  // WORMS_PROJECT_INCLUDE_LISTENERBUTTON_HPP_

