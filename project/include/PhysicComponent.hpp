#ifndef PROJECT_INCLUDE_PHYSICCOMPONENT_HPP_	
#define PROJECT_INCLUDE_PHYSICCOMPONENT_HPP_

#include <SFML/System.hpp>
#include <cmath>
#include <iostream>

#include "quadtree.hpp"
#include "UnitStatus.hpp"
#include "ShellStatus.hpp"
#include "ConstConfig.hpp"

class PhysicComponent {
 public:
	PhysicComponent(UnitStatus* us, QuadTree& qt)
	: mass(Config::getInstance()->WormMass), speed(Config::getInstance()->WormSpeed), accumulatedMove(0, 0), moveVelocity(0,0), velocity(0,0), us(us), qt(qt), moveDown(0) {
	}

	void update(float time);

	bool checkVertical(int mode);

	bool checkHorizontal(int mode, int& findY);

	int move(sf::Vector2i direction);

	void getImpulse(sf::Vector2f umpulsVelocity);

	void handleVelocity(float time);

	void falling(float time);

	void friction(float time);

 private:
 	int mass;
 	float speed;
 	sf::Vector2f accumulatedMove;
 	sf::Vector2f moveVelocity;
	sf::Vector2f velocity;
	UnitStatus* us;
	QuadTree& qt;
	int moveDown;
	int FrictionForceMode;
};



#endif  // PROJECT_INCLUDE_PHYSICCOMPONENT_HPP_
