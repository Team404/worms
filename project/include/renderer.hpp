#ifndef WORMS_PROJECT_INCLUDE_RENDERER_HPP_
#define WORMS_PROJECT_INCLUDE_RENDERER_HPP_

#include <list>

#include "ClientShell.hpp"
#include "Worm.hpp"

class Renderer {
 public:
	Renderer(sf::RenderWindow* window, sf::Texture* map , sf::View* cam, sf::Vector2f size)
	: window(window), map(map), camera(cam), mapSize(size) {
 	}

	void renderQuadTree(QuadTree* qt) {
		if (qt->getState() == 1) {
			sf::Sprite m(*map, sf::IntRect(qt->getData().x,
			                              qt->getData().y,
			                              qt->getData().size,
			                              qt->getData().size));
			m.setPosition(qt->getData().x, qt->getData().y);
			window->draw(m);
		} else if (qt->getState() == -1) {
			for (int i = 0; i < 4; ++i) {
				renderQuadTree(qt->getChild(i));
			}
		}
	}

	void renderWorm(Worm& worm) {
		// std::cout << "1draw" << std::endl;
		/*if (worm.isAlive()) {
			window->draw(worm.getSprite());
			window->draw(worm.getCurrentWeapon().getSprite());
		}*/
		window->draw(worm.getSprite());
		if (worm.isAlive()) {
			window->draw(worm.getCurrentWeapon().getSprite());
			window->draw(worm.getLifeBar().getSprite());
			window->draw(worm.getLifeBar().getBar());
		}
		// std::cout << "2draw" << std::endl;
	}

	void renderShells(std::list<std::unique_ptr<ClientShell>>& shells) {
		for (auto& shell : shells) {
			renderShell(*shell);
		}
	}

	void renderShell(ClientShell& shell) {
		window->draw(shell.getSprite());
	}

	void renderView(UnitStatus& st) {
		int x = st.rect.left + (st.rect.width / 2);
		int y = st.rect.top + (st.rect.height / 2);
		int oldX = camera->getCenter().x;
		int oldY = camera->getCenter().y;
		int newX = oldX;
		int newY = oldY;
		int sizeX = camera->getSize().x;
		int sizeY = camera->getSize().y;
		y -= sizeY / 5;    // делители 5 и 16 - экспериментально полученные значения
		// Плавность камеры
		if (oldX - x > (sizeX / 16)) {
			newX = x + (sizeX / 16);
		}
		if (oldX - x < -(sizeY / 16)) {
			newX = x - (sizeX / 16);
		}
		
		// Проверка на выход за пределы карты
		if (newX < (sizeX / 2)) {
			newX = (sizeX / 2);
		}
		if (newX > mapSize.x - (sizeX / 2)) {
			newX = mapSize.x - (sizeX / 2);
		}

		// Аналогично
		if (oldY - y > (sizeY / 9)) {
			newY = y + (sizeY / 9);
		}
		if (oldY - y < -(sizeY / 9)) {
			newY = y - (sizeY / 9);
		}

		if (newY < (sizeY / 2)) {
			newY = (sizeY / 2);
		}
		if (newY > mapSize.y - (sizeY / 2)) {
			newY = mapSize.y - (sizeY / 2);
		}

		camera->setCenter(newX, newY);
		window->setView(*camera);
	}

	void renderWorms(std::vector<std::unique_ptr<Worm>>& worms) {
		for (auto& worm : worms) {
			// std::cout << "worms" << std::endl;
			renderWorm(*worm);
		}
	}

 private:
 	sf::RenderWindow* window;
 	sf::Texture* map;
	sf::View* camera;
	sf::Vector2f mapSize;
};

#endif  // WORMS_PROJECT_INCLUDE_RENDERER_HPP_
