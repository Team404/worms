#ifndef WORMS_INCLUDE_BUTTON_HPP_
#define WORMS_INCLUDE_BUTTON_HPP_

#include <memory>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

class Button {
 public:
 	Button(std::vector<std::shared_ptr<sf::Sprite>>& sprites, sf::Vector2f pos) 
 	: type(DEFAULT), status(NOT_PRESSED){
 		for (auto& sprite : sprites) {
 			butSprite.emplace_back(sprite);
 		}

 		for (auto& sprite : butSprite) {
 			sprite->setPosition(pos);
 		}
 	}

	enum Type {
		DEFAULT = -1,
		CONNECT = 0,
		QUIT,
		MENU,
		INPUT,
		RESUME,
		DISCONNECT
	};

	enum Status {
		PRESSED = 0,
		NOT_PRESSED,
		WAITING
	};

	sf::Sprite& getSprite() {
		return *butSprite.at(status);
	}

	void setPosition(sf::Vector2f pos) {
		for (auto& sprite : butSprite) {
 			sprite->setPosition(pos);
		}
 		return;
	}
	bool check(sf::Vector2f mousePos) {
		return butSprite[status]->getGlobalBounds().contains(mousePos);
	}

	Type type;
	Status status;

 private:
	std::vector<std::shared_ptr <sf::Sprite>> butSprite;
};

class ButtonFactory {
public:
	ButtonFactory() {
		/*if (!startButton.loadFromFile( "image/start2.png")) {
			std::cerr << "Can't find the image" << std::endl;
		}*/

		if ( !quitButton.loadFromFile("image/exit.png")) {
			std::cerr << "Can't find the image" << std::endl;
		}

		if (!menuButton.loadFromFile("image/pause.png")) {
			std::cout << "Can't find the image" << std::endl;
		}

		if (!inputButton.loadFromFile("image/connect.png")) {
			std::cout << "Can't find the image" << std::endl;
		}
		if (!resumeButton.loadFromFile("image/resume.png")) {
			std::cout << "Can't find the image" << std::endl;
		}
		if (!disconnectButton.loadFromFile("image/disconnect.png")) {
			std::cout << "Can't find the image" << std::endl;
		}
	}

	std::shared_ptr<Button> create(Button::Type type, sf::Vector2f pos) {
		switch(type) {
			/*case Button::CONNECT : {		
				std::vector<std::shared_ptr<sf::Sprite>> sprites; 
				
				sf::Sprite sprite1;
				sprite1.setTexture(startButton);
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite1));

				sf::Sprite sprite2;
				sprite2.setTexture(startButton);
				sprite2.setColor( sf::Color(250, 255, 255));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite2));

				sf::Sprite sprite3;
				sprite3.setTexture(startButton);
				sprite3.setColor(sf::Color(250, 20, 20));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite3));

				auto ret = std::make_shared<Button>(sprites, pos);
				ret->type = type;
				return ret;

			}*/

			case Button::QUIT : {
				std::vector<std::shared_ptr<sf::Sprite>> sprites;
				
				sf::Sprite sprite1;
				sprite1.setTexture(quitButton);
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite1));

				sf::Sprite sprite2;
				sprite2.setTexture(quitButton);
				sprite2.setColor(sf::Color(250, 255, 255));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite2));

				sf::Sprite sprite3;
				sprite3.setTexture(quitButton);
				sprite3.setColor(sf::Color(250, 20, 20));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite3));

				auto ret = std::make_shared<Button>(sprites, pos);
				ret->type = type;
				return ret;
			}

			case Button::MENU : {
				std::vector<std::shared_ptr<sf::Sprite>> sprites;
				
				sf::Sprite sprite1;
				sprite1.setTexture(menuButton);
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite1));

				sf::Sprite sprite2;
				sprite2.setTexture(menuButton);
				sprite2.setColor( sf::Color(250, 255, 255));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite2));

				sf::Sprite sprite3;
				sprite3.setTexture(menuButton);
				sprite3.setColor(sf::Color(250, 20, 20));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite3));

				auto ret = std::make_shared<Button>(sprites, pos);
				ret->type = type;
				return ret;
			}

			case Button::INPUT : {
				std::vector<std::shared_ptr<sf::Sprite>> sprites;
				
				sf::Sprite sprite1;
				sprite1.setTexture(inputButton);
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite1));

				sf::Sprite sprite2;
				sprite2.setTexture(inputButton);
				sprite2.setColor(sf::Color(250, 255, 255));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite2));

				sf::Sprite sprite3;
				sprite3.setTexture(inputButton);
				sprite3.setColor(sf::Color(250, 20, 20));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite3));

				auto ret = std::make_shared<Button>(sprites, pos);
				ret->type = type;
				return ret;
			}

			case Button::RESUME : {
				std::vector<std::shared_ptr<sf::Sprite>> sprites;
				
				sf::Sprite sprite1;
				sprite1.setTexture(resumeButton);
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite1));

				sf::Sprite sprite2;
				sprite2.setTexture(resumeButton);
				sprite2.setColor(sf::Color(250, 255, 255));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite2));

				sf::Sprite sprite3;
				sprite3.setTexture(resumeButton);
				sprite3.setColor(sf::Color(250, 20, 20));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite3));

				auto ret = std::make_shared<Button>(sprites, pos);
				ret->type = type;
				return ret;
			}
			case Button::DISCONNECT : {
				std::vector<std::shared_ptr<sf::Sprite>> sprites;
				
				sf::Sprite sprite1;
				sprite1.setTexture(disconnectButton);
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite1));

				sf::Sprite sprite2;
				sprite2.setTexture(disconnectButton);
				sprite2.setColor(sf::Color(250, 255, 255));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite2));

				sf::Sprite sprite3;
				sprite3.setTexture(disconnectButton);
				sprite3.setColor(sf::Color(250, 20, 20));
				sprites.emplace_back(std::make_shared<sf::Sprite>(sprite3));

				auto ret = std::make_shared<Button>(sprites, pos);
				ret->type = type;
				return ret;
			}
			default : {
				return std::shared_ptr<Button>();	
			}
		}
	}

private:
	//sf::Texture startButton;
	sf::Texture quitButton;
	sf::Texture menuButton;
	sf::Texture inputButton;
	sf::Texture resumeButton;
	sf::Texture disconnectButton;
};

#endif  // WORMS_INCLUDE_BUTTON_HPP_

