#ifndef WORMS_PROJECT_INCLUDE_RESOLUTIONSETS_HPP_
#define WORMS_PROJECT_INCLUDE_RESOLUTIONSETS_HPP_

#include <cstddef>
#include <SFML/System.hpp>

struct defineView {
    float mul;
    size_t XSize;
    size_t YSize;
    defineView(float m, size_t X, size_t Y)
        : mul(m), XSize(X), YSize(Y) {}; 
};

namespace ResConsts {   // Resolution constants
    const defineView R5_4(1.25, 750, 600);
    const defineView R4_3(1.333333, 800, 600);
    const defineView R3_2(1.5, 810, 540);
    const defineView R16_10(1.6, 768, 480);
    const defineView R5_3(1.666666, 800, 480);
    const defineView R16_9(1.777777, 854, 480);
}

// Set camera size
sf::Vector2f resolutionToView(sf::Vector2u windowSize);

#endif  // WORMS_PROJECT_INCLUDE_RESOLUTIONSETS_HPP_
