#ifndef PROJECT_INCLUDE_SERVER_WEAPON_HPP_
#define PROJECT_INCLUDE_SERVER_WEAPON_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <memory>

#include "ServerShell.hpp"
#include "ConstConfig.hpp"


class ServerWeapon {
 public:
 	ServerWeapon(QuadTree& qt, int x, int y)
 	: pos(x, y), reload(0), shootingForce(0), weaponForce(Config::getInstance()->weaponForce), maxForce(Config::getInstance()->maxForce), reloadTime(Config::getInstance()->reloadTime) {
 		factory = std::make_unique<ShellServerFactory>(qt);
 	}

 	void update(int x, int y);

 	void updateReload(float time);

 	void updateShooting(float time);

 	std::unique_ptr<ServerShell> shoot(sf::Vector2f impulse);

 private:
 	sf::Vector2i pos;
 	std::unique_ptr<ShellServerFactory> factory;
 	int reload;
 	int shootingForce;
 	const float weaponForce;
 	const int maxForce;
 	const int reloadTime;
};

#endif  // PROJECT_INCLUDE_SERVER_WEAPON_HPP_
