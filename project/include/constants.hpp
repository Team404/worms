#ifndef WORMS_PROJECT_INCLUDE_CONSTANTS_HPP_
#define WORMS_PROJECT_INCLUDE_CONSTANTS_HPP_

#include <SFML/System.hpp>
//#include "ConstConfig.hpp"

namespace Constants {
	const sf::Int8 QuadTree_ID(1);
	const sf::Int8 Explosion_ID(2);
	const sf::Int8 WormStatus_ID(3);
	const sf::Int8 WormNumber_ID(4);
	const sf::Int8 ShellStatus_ID(5);
	const sf::Int8 Win_ID(6);
}

#endif  // WORMS_PROJECT_INCLUDE_CONSTANTS_HPP_
