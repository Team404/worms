#ifndef PROJECT_INCLUDE_UNIT_HPP_
#define PROJECT_INCLUDE_UNIT_HPP_

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <memory>

//#include "animation_const.hpp"
#include "LifeBar.hpp"
#include "framelists.hpp"
#include "Animations.hpp"
#include "PhysicComponent.hpp"
#include "UnitStatus.hpp"
#include "ClientWeapon.hpp"
#include "ServerWeapon.hpp"
#include "ConstConfig.hpp"

#define PI 3.14159265

template<typename TO, typename FROM>
std::unique_ptr<TO> static_unique_pointer_cast (std::unique_ptr<FROM>&& old){
    return std::unique_ptr<TO>{static_cast<TO*>(old.release())};
}

class Unit {
 public:
	Unit(int x, int y, int width, int height)
	: state(x, y, width, height) {
	}

	virtual void update(float time) = 0; 

	virtual ~Unit() = default;

	bool isAlive() {
		return state.hp > 0;
	}

	UnitStatus& getState() {
		return state;
	}

 private:
	UnitStatus state;	
};

class UnitFactory {
 public:
 	UnitFactory(int width = 24, int height = 26)
 	: width(width), height(height) {
 	}

	virtual std::unique_ptr<Unit> create(int x = 256, int y = 256) = 0;

	virtual ~UnitFactory() = default;
 protected:
 	int width;
 	int height;
};

#endif  // PROJECT_INCLUDE_UNIT_HPP_
