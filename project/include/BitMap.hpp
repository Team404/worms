#ifndef PROJECT_INCLUDE_BITMAP_HPP_
#define PROJECT_INCLUDE_BITMAP_HPP_

#include <cstdint>

// Рекомендуется использовать ширину кратную 8

class BitMap {
 public:
	BitMap(size_t width, size_t height) 
	: width(width), height(height) {
		/**
		 *В случае, если ширина не кратна 8,
		 *необходио выделить еще один бит
		 */
		if (width % 8) {
			++width;
		}

		mass = new uint8_t*[height];
		for (size_t i = 0; i < height; ++i) {
			mass[i] = new uint8_t[width/8];
		}
	}

	BitMap(const sf::Image& img) 
	: BitMap(img.getSize().x, img.getSize().y) {
		for (size_t i = 0; i < img.getSize().y; ++i) {
			for (size_t j = 0; j < img.getSize().x; ++j) {
				if (img.getPixel(i, j).a < 20) {
					set(i, j, 0);
				} else {
					set(i, j, 1);
				}
			}
		}
	}

	~BitMap() {
		for (size_t i = 0; i < height; ++i) {
			delete [] mass[i];
		}	
		delete [] mass;
	}

	bool get(size_t x, size_t y) {
		if (x > width) {
			throw -1;  // TODO: Добавить исключения
		}

		if (y > height) {
			throw -1;
		}

		return mass[y][x/8] & (1 << (7 - x%8));
	}

	void set(size_t x, size_t y, uint8_t elem) {
		if (x > width) {
			throw -1;
		}

		if (y > height) {
			throw -1;
		}

		if (elem) {
			mass[y][x/8] |= (1 << (7 - x%8));
		} else {
			mass[y][x/8] &= ~(1 << (7 - x%8));
		}

	}
 private:
	size_t width;
	size_t height;
	uint8_t** mass;
};

#endif  // PROJECT_INCLUDE_BITMAP_HPP_