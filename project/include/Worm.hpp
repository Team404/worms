#ifndef PROJECT_INCLUDE_WORM_HPP
#define PROJECT_INCLUDE_WORM_HPP

#include "Unit.hpp"
#include "ConstConfig.hpp"


class Worm : public Unit {
 public:
	Worm(sf::Texture& texture, std::vector<std::unique_ptr<Animation>>& animations, int x = 0, int y = 0, int width = 24, int height = 26)
	: Unit(x, y, width, height), lifebar(Config::getInstance()->LifeBarSize), animations(animations) {
		sprite.setTexture(texture);
		sprite.setTextureRect(sf::IntRect(0, 4, width, height));
		weapons.push_back(new ClientWeapon(getState().rect.left + getState().rect.width/2,
		                                   getState().rect.top + getState().rect.height/2));
	}

	sf::Sprite& getSprite();

	void update(float time);

	ClientWeapon& getCurrentWeapon();

	const LifeBar& getLifeBar() const;

 private:
	sf::Sprite sprite;
	LifeBar lifebar;
	std::vector<std::unique_ptr<Animation>>& animations;
	std::vector<ClientWeapon*> weapons;
};


class WormFactory : public UnitFactory {
 public:
	WormFactory(int width = 24, int height = 26)
	: UnitFactory(width, height) {
		texture.loadFromFile("image/worm_move.png");
		
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.012, Config::getInstance()->getFrameInfo().WormStayLeftTile));
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.012, Config::getInstance()->getFrameInfo().WormStayRightTile));
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.05, Config::getInstance()->getFrameInfo().WormLeftTile));
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.05, Config::getInstance()->getFrameInfo().WormRightTile));
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.01, Config::getInstance()->getFrameInfo().WormJumpLeftTile));
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.01, Config::getInstance()->getFrameInfo().WormJumpRightTile));
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.01, Config::getInstance()->getFrameInfo().WormFallLeftTile));
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.01, Config::getInstance()->getFrameInfo().WormFallRightTile));
		animations.push_back(std::make_unique<Animation>(/*sprite,*/ 0.01, Config::getInstance()->getFrameInfo().WormDeadTile));
	}

	std::unique_ptr<Unit> create(int x = 256, int y = 256) {
		return std::make_unique<Worm>(texture, animations, x, y, width, height);
	}
 private:
	sf::Texture texture;
	std::vector<std::unique_ptr<Animation>> animations;
};

#endif  // PROJECT_INCLUDE_WORM_HPP

