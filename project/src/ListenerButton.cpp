#include "ListenerButton.hpp"

void ListenerButton::add(std::shared_ptr<Button>& but) {
		buttons.emplace_back(but);
	}

void ListenerButton::updatePressed(sf::Vector2f mousePos) {
	for (auto& button : buttons) {
		if (button->check(mousePos)) {
			button->status = Button::PRESSED;
			updateEvent(*button);
		}
	}
}

void ListenerButton::updateWaiting(sf::Vector2f mousePos) {
	for (auto& button : buttons) {
		if (button->check(mousePos)) {
			button->status = Button::WAITING;
			updateEvent(*button);
		} else {
			button->status = Button::NOT_PRESSED;
		}
	}
}

void ListenerButton::updateEvent(Button& but) {
	event.status = but.status;
	event.type = but.type;
}

ButtonEvent ListenerButton::getEvent() {
	ButtonEvent tmp = event;
	event.type = Button::DEFAULT;
	return tmp;
}