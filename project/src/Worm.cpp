#include "Worm.hpp"

sf::Sprite& Worm::getSprite() {
	return sprite;
}

void Worm::update(float time) {
	// std::cout << getState() << std::endl;  // DEBUG
	if (isAlive()) {
		switch (getState().onGround) {
			case 4:  // Карабкается
			case 0: {  // На земле
				switch (getState().moveRight - getState().moveLeft) {
					case 0: {  // Стоит на месте
						if (getState().direction > 90 && getState().direction < 270) {
							// sprite.setTextureRect(sf::IntRect(0, 4, 26, 26));
							sprite = animations[Config::getInstance()->getAnime().StayLeft]->showFrame(time);
						} else {
							// sprite.setTextureRect(sf::IntRect(26, 4, -26, 26));
							sprite = animations[Config::getInstance()->getAnime().StayRight]->showFrame(time);
						}
						break;
					}

					case 1: {  // Идет вправо
						sprite = animations[Config::getInstance()->getAnime().MoveRight]->showFrame(/*sprite, */time);
						break;
					}

					case -1: {  // Идет влево
						sprite = animations[Config::getInstance()->getAnime().MoveLeft]->showFrame(/*sprite, */time);
						break;
					}
				}
			}
				
			case 1: {  // Готовится к прыжку
				// TODO: Добавить обработку
				break;
			}

			case 2: {  // Летит
				if (getState().direction > 90 && getState().direction < 270) {
					sprite = animations[Config::getInstance()->getAnime().JumpLeft]->showFrame(/*sprite, */time);
				} else {
					sprite = animations[Config::getInstance()->getAnime().JumpRight]->showFrame(/*sprite, */time);
				}
				break;
			}
			
			case 3: {  // Падает
				if (getState().direction > 90 && getState().direction < 270) {
					sprite = animations[Config::getInstance()->getAnime().FallLeft]->showFrame(/*sprite, */time);
				} else {
					sprite = animations[Config::getInstance()->getAnime().FallRight]->showFrame(/*sprite, */time);
				}
				break;
			}
		}
	} else {
		sprite = animations[Config::getInstance()->getAnime().Dead]->showFrame(time);
	}
	sprite.setPosition(getState().rect.left, getState().rect.top);
	getCurrentWeapon().update(getState().rect.left + getState().rect.width/2,
	                          getState().rect.top + getState().rect.height/2,
	                          getState().direction);

	lifebar.update(getState().hp, getState().rect);
	// std::cout << getState().direction << std::endl;
}

ClientWeapon& Worm::getCurrentWeapon() {
	return *weapons[getState().currentWeapon];
}

const LifeBar& Worm::getLifeBar() const {
	return lifebar;
}
