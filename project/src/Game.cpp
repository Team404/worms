#include <SFML/Network.hpp>
#include <iostream>
#include <cstdint>
#include <cmath>
#include <memory>

#include "Unit.hpp"
#include "Animations.hpp"
#include "BitMap.hpp"
#include "constants.hpp"
#include "quadtree.hpp"
#include "renderer.hpp"
#include "ResolutionSets.hpp"
#include "ClientPacketHandler.hpp"
#include "Button.hpp"
#include "ListenerButton.hpp"
#include "TextBar.hpp"
#include "Parser.hpp"
#include "Game.hpp"
#include "Client.hpp"

double mouseAngle(const float dx, const float dy) {
	float tan = 0;
	float angle = 0;
	if (dy <= 0) {
		if (dx > 0) {
			tan = dy / dx;
		}
		if (dx < 0) {
			tan = dx / dy;
			angle += 90;
		}
		if (dx == 0) {
			tan = 0;
		}
	}
	if (dy > 0) {
		if (dx < 0) {
			tan = dy / dx;
			angle += 180;
		} else {
			tan = dx / dy;
			angle += 270;
		}
	}
	angle += atan(fabs(tan)) * 180 / 3.14;
	return angle;
}

void startScreen(sf::RenderWindow& window) {
	sf::Texture logoTexture;
	if (!logoTexture.loadFromFile("image/spiral3.png")) {
		std::cout << "Error: Could not display logo image" << std::endl;
	}
	sf::Sprite logo;
	logo.setTexture(logoTexture);
	logo.setOrigin(logo.getTextureRect().width / 2, logo.getTextureRect().height / 2);
	logo.setPosition(window.getSize().x / 2, window.getSize().y / 2);
	ShadowAnimation logoShadow(0.0002, sf::Vector2f(window.getSize().x, window.getSize().y), sf::Color::Black, 255);
	sf::Clock clock;
	float time = 0;
	while (!logoShadow.isAlpha()) {
		window.clear();
		sf::Time sftime = clock.getElapsedTime();
		time = sftime.asMicroseconds();
		clock.restart();
		window.draw(logo);
		window.draw(logoShadow.showDownFrame(time));
		window.display();
	}
	sf::Time slp = sf::seconds(1.5);
	sf::sleep(slp);
	clock.restart();
	while (!logoShadow.isFill()) {
		window.clear();
		sf::Time sftime = clock.getElapsedTime();
		time = sftime.asMicroseconds();
		clock.restart();
		window.draw(logo);
		window.draw(logoShadow.showUpFrame(time));
		window.display();
	}
	slp = sf::seconds(0.25);
	sf::sleep(slp);
	return;
}

Game::ButtonManager::ButtonManager(sf::View& camera, sf::Font& font, ButtonFactory& factory) 
	: isPause(false), isQuit(false), exitMenu(sf::Vector2f(400,200)), loseMsg("You lose!", font, 50),
	  winMsg("You win!", font, 50)
{
	exitMenu.setFillColor(sf::Color::White);
	exitMenu.setPosition(sf::Vector2f(camera.getCenter().x - 200, camera.getCenter().y - 100));
	sf::Color clr = exitMenu.getFillColor();
	clr.a = 140;
	exitMenu.setFillColor(clr);
	loseMsg.setFillColor(sf::Color(255, 84, 74));
	loseMsg.setStyle(sf::Text::Bold);
	winMsg.setFillColor(sf::Color(173, 160, 251));
	winMsg.setStyle(sf::Text::Bold);
	gameButtons.emplace_back(factory.create(Button::MENU, sf::Vector2f(camera.getCenter().x + 380,
				             camera.getCenter().y - 250)));
	for (auto& button : gameButtons) {
		listenerGame.add(button);
	}
	pauseButtons.emplace_back(factory.create(Button::RESUME, sf::Vector2f(camera.getCenter().x + 90,
				              camera.getCenter().y - 80)));
	pauseButtons.emplace_back(factory.create(Button::DISCONNECT, sf::Vector2f(camera.getCenter().x + 90,
				              camera.getCenter().y + 20)));
	for (auto& button : pauseButtons) {
		listenerPause.add(button);
	}
}

void Game::ButtonManager::handleButton(Button::Type typeButton) {
	switch (typeButton) {
		case Button::MENU : {
			isPause = true;
			break;
		}
		case Button::RESUME : {
			isPause = false;
			break;
		}
		case Button::DISCONNECT : {
			isQuit = true;
			break;
		}
		default : {
			break;
		}
	}
}

Game::Game() : window(sf::VideoMode::getDesktopMode(), "worms"),
			   shadow(0.0008, static_cast<sf::Vector2f>(window.getSize()), sf::Color::Black, 80)
{
	if ( !fon.loadFromFile("image/start.png")) {
		std::cout << "Error: Could not display testfon image" << std::endl;
	}
	fonImage.setTexture(fon);
	float mul = float(window.getSize().x) / float(window.getSize().y);
	sf::IntRect backgroundRect(0, 426 * (mul - 1.25), 1024, 1024.0 / mul);

	fonImage.setTextureRect(backgroundRect);
	fonImage.scale(sf::Vector2f(float(window.getSize().x) / 1024.0, float(window.getSize().x) / 1024.0));

	fc.setFocusObject(&tb);	
	buttons.emplace_back(factory.create(Button::QUIT, sf::Vector2f(window.getSize().x / 2 - 50, window.getSize().y / 1.6 + 80)));
	buttons.emplace_back(factory.create(Button::INPUT, sf::Vector2f(window.getSize().x / 2 + 100, window.getSize().y / 1.6)));
	font.loadFromFile("fonts/minecraft.ttf");
	text.setFont(font);
	errmsg.setFont(font);
	if (!input.loadFromFile("image/input_big.png")) {
		std::cout << "Error: Could not display input image" << std::endl;
	}
	inputImage.setTexture(input);
	inputImage.setPosition(sf::Vector2f(window.getSize().x / 2 - 240, window.getSize().y / 1.6));
	for (auto& button : buttons) {
		listenButton.add(button);
	}
	startScreen(window);
}

void Game::handleEvent(sf::Event event, ListenerButton& listener) {
	sf::Vector2i mousePos = static_cast<sf::Vector2i>(window.mapPixelToCoords(sf::Mouse::getPosition(window)));
	sf::Vector2f mousePosF( static_cast<float>( mousePos.x ), static_cast<float>( mousePos.y ) );
	if (event.type == sf::Event::MouseMoved || event.type == sf::Event::MouseButtonReleased) {
		listener.updateWaiting(mousePosF);
	}
	if (event.type == sf::Event::MouseButtonPressed) {
		listener.updatePressed(mousePosF);
	}
}

bool Game::update() {
	sf::Event event;
	while (window.pollEvent(event)) {
		if (event.type == sf::Event::Closed) {
			window.close();
			return false;
		}

		FocusObject* fo = fc.getFocusObject();
		if (fo != 0) {
					fo->event(event);
				}
		handleEvent(event, listenButton);
	}

	ButtonEvent butEvent = listenButton.getEvent();
	if (butEvent.status == Button::PRESSED) {
		switch (butEvent.type) {
			case Button::INPUT : {
				//IpPort = tb.getString();
				window.clear();
				cl = std::make_unique<Client>();
				unsigned port = 0;
				Parser data(tb.getText());
				sf::IpAddress ip = sf::IpAddress::None;
				try {
					ip = data.getIp();
					port = data.getPort();
				} catch (std::out_of_range& excep) {
					std::cerr << "incorret port" << std::endl;
					errmsg.setString("incorret port");
					window.draw(errmsg);
				} catch (std::invalid_argument& exept) {
					std::cerr << "incorret port" << std::endl;
					errmsg.setString("incorret port");
					window.draw(errmsg);
				}
				if (cl->start(ip, port, window, fonImage, 1024)) {
					shadow.setAlpha(0);
					sf::Clock clock;
					float time = 0;

					while (!shadow.isFill()) {
						window.clear();
						sf::Time sftime = clock.getElapsedTime();
						time = sftime.asMicroseconds();
						clock.restart();
						window.draw(fonImage);
						for (auto& button : buttons) {
							window.draw(button->getSprite());
						}
						window.draw(shadow.showUpFrame(time));
						window.display();
					}
					sf::Time slp = sf::seconds(0.07);
					sf::sleep(slp);
					play();
					errmsg.setString(" ");
				} else {
					errmsg.setString("Incorrect ip");
					std::cout << "Incorrect ip" << std::endl;
				}
				window.draw(errmsg);
				cl.reset();
				break;
			}

			case Button::QUIT : {
				window.close();
				return false;
			}
				
			default: {
				break;
			}
		}
	}
	text = tb.getText();
	window.clear();
	window.draw(fonImage);
	for (auto& button : buttons) {
		window.draw(button->getSprite());
	}
	window.draw(inputImage);
	text.setPosition(sf::Vector2f(window.getSize().x / 2 - 234 , window.getSize().y / 1.6 + 16));
	text.setCharacterSize(21);
	text.setFillColor(sf::Color::Black);
	errmsg.setFillColor(sf::Color::Red);
	errmsg.setPosition(sf::Vector2f(window.getSize().x / 2 - 220, window.getSize().y / 1.6 + 180));

	errmsg.setFont(font);
	window.draw(errmsg);
	text.setFont(font);
	window.draw(text);
	window.display();

	return true;
}

void Game::play() {
	sf::View camera(sf::Vector2f(0, 0), resolutionToView(window.getSize()));
		
	sf::Texture backgroundTexture;
	backgroundTexture.loadFromFile("image/vyetnam_fon.png");
	sf::Sprite background(backgroundTexture);
		
	sf::Texture map;
	map.loadFromFile("image/vyetnam.png");

	Renderer renderer(&window, &map, &camera, sf::Vector2f(1024, 1024));
	
	shadow.setAlpha(80);
	
	sf::Clock clock;
	sf::Time fixed = sf::Time::Zero;
	sf::Time timePerFrame = sf::seconds(1 / 30.0f);
	ButtonManager buttonManager(camera, font, factory);
	while (true) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (!buttonManager.isPause) {
				cl->handleEvent(event, window);
			}
			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Escape) {
					buttonManager.isPause = !buttonManager.isPause;
				}
			}
			if (event.type == sf::Event::Resized) {
				camera.reset(sf::FloatRect(sf::Vector2f(0, 0),
										   resolutionToView(window.getSize())));
			}
		}
		
		sf::Time sftime = clock.getElapsedTime();
		clock.restart();

		fixed = fixed + sftime;
		if (fixed.asSeconds() > 1.0f / 30) {
			fixed = sf::Time::Zero;
			cl->sendState();
		}
		if (sftime > timePerFrame) {
			cl->update(timePerFrame.asMicroseconds());
			sftime -= timePerFrame;
		}
		cl->update(sftime.asMicroseconds());
		handleEvent(event, buttonManager.isPause ? buttonManager.listenerPause : buttonManager.listenerGame);
		buttonManager.update();

		window.clear();
		window.draw(background);
		renderer.renderQuadTree(&cl->getState().getQT());
		renderer.renderWorms(cl->getState().getWorms());
		renderer.renderShells(cl->getState().getShells());

		switch (cl->getState().getWin()) {
			case 1 : {
				buttonManager.winMsg.setPosition(camera.getCenter().x - 100, camera.getCenter().y - 25);
				window.draw(shadow.showFrame());
				window.draw(buttonManager.winMsg);
				break;
			}
			case -1 : {
				buttonManager.loseMsg.setPosition(camera.getCenter().x - 100, camera.getCenter().y - 25);
				window.draw(shadow.showFrame());
				window.draw(buttonManager.loseMsg);
				break;
			}
		}
			
		if (buttonManager.isPause) {
			buttonManager.exitMenu.setPosition(sf::Vector2f(camera.getCenter().x - 200, camera.getCenter().y - 100));
			buttonManager.pauseButtons.at(0)->getSprite().setPosition(sf::Vector2f(camera.getCenter().x - 90, camera.getCenter().y - 80));
			buttonManager.pauseButtons.at(1)->getSprite().setPosition(sf::Vector2f(camera.getCenter().x - 90, camera.getCenter().y + 20));
			window.draw(buttonManager.exitMenu);
			for (auto& button : buttonManager.pauseButtons) {
				window.draw(button->getSprite());
			}
		} else {
			buttonManager.gameButtons.at(0)->setPosition(sf::Vector2f(camera.getCenter().x + 380, camera.getCenter().y - 230));
			for (auto& button : buttonManager.gameButtons) {
				window.draw(button->getSprite());
			}
		}
		if (buttonManager.isQuit) {
			shadow.setAlpha(80);
			while (!shadow.isFill()) {
				sftime = clock.getElapsedTime();
				clock.restart();
				window.clear();
				window.draw(background);
				renderer.renderQuadTree(&cl->getState().getQT());
				renderer.renderWorms(cl->getState().getWorms());
				renderer.renderShells(cl->getState().getShells());
			
				if (sftime > timePerFrame) {
					window.draw(shadow.showUpFrame(timePerFrame.asMicroseconds()));
					sftime -= timePerFrame;
				}
				window.draw(shadow.showUpFrame(sftime.asMicroseconds()));
				window.display();
			}
			sf::Time slp = sf::seconds(0.2);
			sf::sleep(slp);
			window.setView(window.getDefaultView());
			return;	
		}
		renderer.renderView(cl->getState().getPlayerState());
		window.display();
	}
}
