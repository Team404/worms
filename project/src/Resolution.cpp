#include "ResolutionSets.hpp"
#include <vector>
#include <SFML/System.hpp>
#include <iostream>

std::vector<defineView> views = {
	ResConsts::R5_4,
	ResConsts::R4_3,
	ResConsts::R3_2,
	ResConsts::R16_10,
	ResConsts::R5_3,
	ResConsts::R16_9
};

sf::Vector2f resolutionToView(sf::Vector2u windowSize) {

	float winMul = float(windowSize.x) / float(windowSize.y);
	sf::Vector2f cameraSize;

	float left = 0;
	float right = 0;
	bool widescreen = true;

	// Перебор разрешений камеры в сответствии с разрешением
	// пользовательского экрана
	for (size_t i = 0; i < views.size() - 1; ++i) {
		left = right;
		right = (views[i + 1].mul + views[i].mul) / 2;
		if ((left < winMul) && (winMul < right)) {
			cameraSize.x = views[i].XSize;
			cameraSize.y = views[i].YSize;
			widescreen = false;
			break;
		}
	}
	// Если широкоформатный
	if (widescreen) {
		cameraSize.x = ResConsts::R16_9.XSize;
		cameraSize.y = ResConsts::R16_9.YSize;
	}
	
	return cameraSize;
}
