#include "ClientPacketHandler.hpp"
#include "Worm.hpp"

void ClientPacketHandler::handleExplosion(sf::Packet& packet) {
	int x = 0;
	int y = 0;
	int r = 0;
	packet >> x >> y >> r;
	state.explosion(x, y, r);
}

void ClientPacketHandler::handleStatus(sf::Packet& packet) {
	unsigned size = 0;
	packet >> size;
	std::vector<std::unique_ptr<UnitStatus>> states;
	for (unsigned i = 0; i < size; ++i) {
		std::unique_ptr<UnitStatus> tmp = std::make_unique<UnitStatus>();
		packet >> *tmp;
		states.push_back(std::move(tmp));
	}
	state.updateWormState(states);
}

void ClientPacketHandler::handleQT(sf::Packet& packet) {
	packet >> state.getQT();
}

void ClientPacketHandler::handleNumber(sf::Packet& packet) {
	int number = 1;
	packet >> number;
	std::cout << "number is " << number << std::endl;
	state.setWormNumber(number);
}

void ClientPacketHandler::handleShells(sf::Packet& packet) {
	unsigned size = 0;
	packet >> size;
	state.getShells().resize(size);
	for (auto& shell : state.getShells()) {
		if (!shell) {
			shell = std::move(shellFactory.create());
		}
		packet >> shell->getState();
	}
}

void ClientPacketHandler::handleWin(sf::Packet& packet) {
	int win = 0;
	packet >> win;
	state.setWin(win);
}