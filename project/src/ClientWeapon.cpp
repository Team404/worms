#include "ClientWeapon.hpp"

void ClientWeapon::update(int x ,int y, float angle) {
	sprite.setPosition(x, y);
	sprite.setRotation(-angle);
}

sf::Sprite& ClientWeapon::getSprite() {
	return sprite;
}