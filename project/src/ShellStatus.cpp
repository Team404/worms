#include "ShellStatus.hpp"

sf::Packet& operator <<(sf::Packet& packet, const ShellStatus& state) {
	packet << state.rect << state.direction << state.radius << state.explose;
	return packet;
}

sf::Packet& operator >>(sf::Packet& packet, ShellStatus& state) {
	packet >> state.rect >> state.direction >> state.radius >> state.explose;
	return packet;
}