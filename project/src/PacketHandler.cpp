#include "PacketHandler.hpp"

void PacketHandler::handlePacket(sf::Packet& packet) {
	sf::Int8 id = 0;
	packet >> id;
	switch (id) {
		case Constants::Explosion_ID : {
			handleExplosion(packet);
			break;
		}

		case Constants::WormStatus_ID : {
			handleStatus(packet);
			break;
		}

		case Constants::WormNumber_ID : {
			handleNumber(packet);
			break;
		}

		case Constants::QuadTree_ID : {
			handleQT(packet);
			break;
		}

		case Constants::ShellStatus_ID : {
			handleShells(packet);
			break;
		}

		case Constants::Win_ID : {
			handleWin(packet);
			break;
		}

		default: {
			std::cout << "Error : " << int(id) << std::endl;
		}
	}
}

void PacketHandler::handleVector(std::vector<std::unique_ptr<sf::Packet>>& packets) {
	for (auto& packet : packets) {
		handlePacket(*packet);
	}
}
