#include "Server.hpp"
#include "BitMap.hpp"
#include "constants.hpp"

void Server::start() {
	if (!port) {
 		if (listener.listen(listener.AnyPort) != sf::Socket::Done) {
			exit(-1);
		}
		port = listener.getLocalPort();
	} else {
		if (listener.listen(port) != sf::Socket::Done) {
			exit(-1);
		}
	}
		selector.add(listener);
	
	std::cout << "Server ip: " << sf::IpAddress::getLocalAddress() << std::endl;
	std::cout << "Port: " << port << std::endl;

	gamestate = std::make_unique<GameState>(qtSize, clientNum);
	ph = std::make_unique<ServerPacketHandler>(*gamestate);
	
	while (clientNum != clients.size()) {
		if (selector.wait()) {
			std::unique_ptr<sf::TcpSocket> client = std::make_unique<sf::TcpSocket>();
			if (listener.accept(*client) == sf::Socket::Done) {
				selector.add(*client);
				sf::Packet packet;
				packet << Constants::QuadTree_ID << gamestate->getQT();
				std::cout << "Client connected: " << client->getRemoteAddress() << std::endl;
				client->send(packet);
				client->setBlocking(false);
				clients.push_back(std::move(client));
				++clientConnected;
			}
		}
	}

	listener.close();

	pushWormsStatus();
	sendOutput();
	clear();
	sendWormNumber();
}

void Server::update(float time) {
	gamestate->update(time);
	for(auto& expl : gamestate->getExplosions()) {
		std::unique_ptr<sf::Packet> packet = std::make_unique<sf::Packet>();
		*packet << Constants::Explosion_ID << expl.x << expl.y << expl.r;
		outputPackets.push_back(std::move(packet));
	}
	gamestate->handleExplosions();
	gamestate->clearExplosions();
	/*if (!recieveAndAnswer()){
	}*/
}

void Server::updateWin() {
	if (gamestate->getCountAlive() < gamestate->getPlayerCount()) {
		unsigned i = 0;
		sf::Packet packet;
		for (auto& worm : gamestate->getWorms()) {
			if (!worm->isAlive() && i <= clients.size() && gamestate->getCountAlive() > 0) {
				packet.clear();
				packet << Constants::Win_ID << -1;
				clients.at(i)->send(packet);
			}
			++i;
		}
		if (gamestate->getCountAlive() == 1) {
			int i = 0;
			for (auto& worm : gamestate->getWorms()) {
				if (worm->isAlive()) {
					packet.clear();
					packet << Constants::Win_ID << 1;
					clients.at(i)->send(packet);
				}
				++i;
			}
		}
	}
}

bool Server::recieve() {
	std::vector<std::unique_ptr<sf::Packet>> packets;
	bool ret = false;
	for(auto& client : clients) {
		sf::Packet packet;
		while (client->receive(packet) == sf::Socket::Done) {
			// std::cout << "received" << std::endl;
			packets.emplace_back(std::make_unique<sf::Packet>(packet));
			ret = true;
		}
	}
	if (ret) {
		ph->handleVector(packets);
	}
	return ret;
}

void Server::pushWormsStatus() {
	std::unique_ptr<sf::Packet> packet = std::make_unique<sf::Packet>();
	*packet << Constants::WormStatus_ID << static_cast<unsigned>(gamestate->getWorms().size());
	for (auto& worm : gamestate->getWorms()) {
		*packet << worm->getState();
	}
	outputPackets.push_back(std::move(packet));
}

void Server::pushShellStatus() {
	std::unique_ptr<sf::Packet> packet = std::make_unique<sf::Packet>();
	*packet << Constants::ShellStatus_ID << static_cast<unsigned>(gamestate->getShells().size());
	for (auto& shell : gamestate->getShells()) {
		*packet << shell->getState();
	}
	outputPackets.push_back(std::move(packet));
}

void Server::sendWormNumber() {
	sf::Packet packet;
	int i = 0;
	for (auto& client : clients) {
		packet << Constants::WormNumber_ID << i;
		std::cout << "send number " << i << " to " << client->getRemoteAddress()<<  std::endl;
		client->send(packet);
		packet.clear();
		++i;
	}
}

void Server::sendOutput() {
	for (auto& packet : outputPackets) {
		sendPacket(*packet);
	}
}

void Server::sendPacket(sf::Packet& packet) {
	for (auto& client : clients) {
		sf::Socket::Status state;
		do {
			state = client->send(packet);
		} while (state == sf::Socket::Partial);

		if (state == sf::Socket::Disconnected) {
			std::cout << client->getRemoteAddress() << " has been disconnected" << std::endl;
			client->disconnect();
			--clientConnected;
		}
		// std::cout << packet.getDataSize() << std::endl;
	}

	if (!clientConnected) {
		end = true;
	}
}

bool Server::getEnd() {
	return end;
}

void Server::clear() {
	outputPackets.clear();
}

void Server::restartRes() {
	selector.clear();
	clients.clear();
	gamestate.reset();
	ph.reset();
	outputPackets.clear();
	end = false;
}