#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>

#include "animation_const.hpp"
#include "framelists.hpp"
#include "UnitStatus.hpp"

class Animation {
	private:
		float CurrentFrame;
		float MoveFrame;
		// sf::Sprite my_sprite;
		const Frame frames; 
	public:
		// Animation() {};
		Animation(/*sf::Sprite& sprite, */float move_time, const Frame& list)
			:/* my_sprite(sprite), */MoveFrame(move_time), frames(list) {
			CurrentFrame = 0;
		}
		// virtual void showFrame(sf::Sprite&, float) = 0;
		void showFrame(sf::Sprite& sprite, float time) {
			CurrentFrame += MoveFrame * time;
			if (CurrentFrame > frames.num_frames) {
				CurrentFrame -= frames.num_frames;
			}
			sprite.setTextureRect(sf::IntRect(frames.x + int(CurrentFrame)*abs(frames.dx),
			                                  frames.y,
			                                  frames.dx,
			                                  frames.dy));
		}
};

class Worm {
	private:
		sf::Sprite sprite;
		std::vector<Animation*> Animations;
		UnitStatus state;
	public:
		Worm(sf::Sprite Spr, int x = 0, int y = 0, int width = 26, int height = 26) : sprite(Spr), state(x, y, width, height) { 
			sprite.setTextureRect(sf::IntRect(0, 4, width, height));
			Animations.push_back(new Animation(/*sprite,*/ 0.01, FrameList::WormLeftTile));
			Animations.push_back(new Animation(/*sprite,*/ 0.01, FrameList::WormRightTile));
			Animations.push_back(new Animation(/*sprite,*/ 0.01, FrameList::WormJumpLeftTile));
			Animations.push_back(new Animation(/*sprite,*/ 0.01, FrameList::WormJumpRightTile));
			Animations.push_back(new Animation(/*sprite,*/ 0.01, FrameList::WormFallLeftTile));
			Animations.push_back(new Animation(/*sprite,*/ 0.01, FrameList::WormFallRightTile));
		}
		sf::Sprite& getSprite() {
			return sprite;
		}

		UnitStatus& getState() {
			return state;
		}

		void update(float time) {
			switch (state.onGround) {
				case 0: {
					switch (state.moveRight - state.moveLeft) {
						case 0: {
							if (state.direction == 180) {
								sprite.setTextureRect(sf::IntRect(0, 4, 26, 26));
							} else {
								sprite.setTextureRect(sf::IntRect(26, 4, -26, 26));
							}
							break;
						}

						case 1: {
							Animations[AnimationConsts::MoveRight]->showFrame(sprite, time);
							break;
						}

						case -1: {
							Animations[AnimationConsts::MoveLeft]->showFrame(sprite, time);
							break;
						}

					}
				}
					
				case 1: {
					// TODO: Добавить обработку
					break;
				}

				case 2: {
					if (state.direction == 180) {
						Animations[AnimationConsts::JumpLeft]->showFrame(sprite, time);
					} else {
						Animations[AnimationConsts::JumpRight]->showFrame(sprite, time);
					}
					break;
				}
				
				case 3: {
					if (state.direction == 180) {
						Animations[AnimationConsts::FallLeft]->showFrame(sprite, time);
					} else {
						Animations[AnimationConsts::FallRight]->showFrame(sprite, time);
					}
					break;
				}
			}
			sprite.setPosition(state.rect.left, state.rect.top);
		}
};

/*
int main() {
	sf::RenderWindow window(sf::VideoMode(800, 600), "Test");

	sf::Sprite sprite;
	sf::Texture texture;
	texture.loadFromFile("../../image/worm_move.png");
	sprite.setTexture(texture);
	
	Worm w1(sprite, 250, 250, 26, 26); 
	window.setFramerateLimit(60);  // NOTE: I'm not sure, that I need it

	sf::Clock clock;

	// float wormspeed = 0.11;
	// float wormFrame = 0.01;
	while (window.isOpen()) {
		float time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time/1600;  // 800 - Recommended
		sf::Event event;
		while (window.pollEvent(event)) {
			if ((event.type == sf::Event::Closed) ||
				(event.type == sf::Event::KeyPressed &&
				event.key.code == sf::Keyboard::Escape)) {
				window.close();
			}
			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Left) {
	 				/*state = left;
					speed = 0.11;
					w1.getState().moveLeft = 1;
					w1.getState().direction = 180;
				}
				if (event.key.code == sf::Keyboard::Right) {
					w1.getState().moveRight = 1;
					w1.getState().direction = 0;
				}
				if (event.key.code == sf::Keyboard::Space) {
					w1.getState().onGround = 2;
				}
				// For test
				if (event.key.code == sf::Keyboard::LControl) {
					w1.getState().onGround = 3;
				}  // For test
			}
			if (event.type == sf::Event::KeyReleased) {
				if (event.key.code == sf::Keyboard::Left) {
	 				/*state = left;
					speed = 0.11;
					*
					w1.getState().moveLeft = 0;
				}
				if (event.key.code == sf::Keyboard::Right) {
					w1.getState().moveRight = 0;
				}
				if (event.key.code == sf::Keyboard::Space) {
					w1.getState().onGround = 0;
				}
				// For test
				if (event.key.code == sf::Keyboard::LControl) {
					w1.getState().onGround = 0;
				}  // For test
			}
			if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Space))/* && (onGround)) {
				/*state = jump;
				dy = -0.4;
				onGround = false;
				
			}
		}
		
			
		window.clear();
		w1.update(time);
		window.draw(w1.getSprite());
		window.display();
	}

	return 0;
}
*/
