#include "ConstConfig.hpp"
#include <cstdlib>

AnimationConsts Config::getAnime() const {
	return anime;
}

FrameInfo Config::getFrameInfo() const {
	return frinfo;
}

Config::~Config() {  // хотя, судя по тому, что я прочитал на stackoverflow этот деструктор вызываться не будет, но вроде как это решается если сделать метод получения id константным 
	delete myConfig;
}

Config* Config::myConfig = nullptr;

Config* Config::getInstance() {
	if (!myConfig) {
		myConfig = new Config;
		myConfig->openConfigFile("config");
	}
	return myConfig;
}

int Config::hashit (std::string const& name) {
	if (name == "StayLeft=") {
		return StayLeft;
	} else if (name == "StayRight=") {
		return StayRight;
	} else if (name == "MoveLeft=") {
		return MoveLeft;
	} else if (name == "MoveRight=") {
		return MoveRight;
	} else if (name == "JumpLeft=") {
		return JumpLeft;
	} else if (name == "JumpRight=") {
		return JumpRight;
	} else if (name == "FallLeft=") {
		return FallLeft;
	} else if (name == "FallRight=") {
		return FallRight;
	} else if (name == "Dead=") {
		return Dead;
	} else if (name == "Gravity=") {
		return Gravity_ID;
	} else if (name == "GameSpeed=") {
		return GameSpeed_ID;
	} else if (name == "FrictionCoefficient=") {
		return FrictionCoefficient_ID;
	} else if (name == "Space=") {
		return Space_ID;
	} else if (name == "WormSpeed=") {
		return WormSpeed_ID;
	} else if (name == "WormMass=") {
		return WormMass_ID;
	} else if (name == "ShellMass=") {
		return ShellMass_ID;
	} else if (name == "LifeBarSize=") {
		return LifeBarSize_ID;
	} else if (name == "WormStayLeftTile=") {
		return WormStayLeftTile_ID;
	} else if (name == "WormStayRightTile=") {
		return WormStayRightTile_ID;
	} else if (name == "WormLeftTile=") {
		return WormLeftTile_ID;
	} else if (name == "WormRightTile=") {
		return WormRightTile_ID;
	} else if (name == "WormJumpLeftTile=") {
		return WormJumpLeftTile_ID;
	} else if (name == "WormJumpRightTile=") {
		return WormJumpRightTile_ID;
	} else if (name == "WormFallLeftTile=") {
		return WormFallLeftTile_ID;
	} else if (name == "WormFallRightTile=") {
		return WormFallRightTile_ID;
	} else if (name == "WormDeadTile=") {
		return WormDeadTile_ID;
	} else if (name == "weaponForce=") {
		return weaponForce_ID;
	} else if (name == "maxForce=") {
		return maxForce_ID;
	} else if (name == "reloadTime=") {
		return reloadTime_ID;
	} else if (name == "respawnPoint1=") {
		return respawnPoint1;
	} else if (name == "respawnPoint2=") {
		return respawnPoint2;
	} else if (name == "respawnPoint3=") {
		return respawnPoint3;
	} else if (name == "respawnPoint4=") {
		return respawnPoint4;
	} else if (name == "waitingTime=") {
		return waitingTime_ID;
	} else if (name == "weaponDamage=") {
		return weaponDamage_ID;
	} else {
		return -1;
	}
};

bool Config::openConfigFile(const char* path_file) {
	std::ifstream config(path_file);
	try {
		if (!config.is_open()) {
			throw 228;
		}
	} catch (int f) {
		std::cerr << "File wasn't open, code: " << f << std::endl;
		return false;
	}
	for (std::string line; std::getline(config, line); ) {
		std::istringstream in(line);
		std::string name;
		in >> name;
		switch (hashit(name)) {
			case StayLeft : {
				int num = 0;
				in >> num; 
				anime.StayLeft = num;
				break;
			}
			case MoveLeft : {
				int num = 0;
				in >> num;
				anime.MoveLeft = num;
				break;
			}
			case StayRight : {
				int num = 0;
				in >> num;
				anime.StayRight= num;
				break;
			}
			case MoveRight : {
				int num = 0;
				in >> num;
				anime.MoveRight = num;
				break;
			}
			case JumpLeft : {
				int num = 0;
				in >> num; 
				anime.JumpLeft = num;
				break;
			}
			case JumpRight : {
				int num = 0;
				in >> num; 
				anime.JumpRight = num;
				break;
			}
			case FallLeft : {
				int num = 0;
				in >> num; 
				anime.FallLeft = num;
				break;
			}
			case FallRight : {
				int num = 0;
				in >> num;
				anime.FallRight = num;
				break;
			}
			case Dead : {
				int num = 0;
				in >> num; 
				anime.Dead = num;
				break;
			}
			case Gravity_ID : {
				float num1 = 0;
				float num2 = 0;
				in >> num1 >> num2;
				Gravity.x = num1;
				Gravity.y = num2;
				break;
			}
			case Space_ID : {
				float num1 = 0;
				float num2 = 0;
				in >> num1 >> num2;
				Space.x = num1;
				Space.y = num2;
				break;
			}
			case GameSpeed_ID : {
				float num = .0;
				in >> num;
				GameSpeed = num;
				break;
			}
			case FrictionCoefficient_ID : {
				float num = .0;
				in >> num;
				FrictionCoefficient = num;
				break;
			}
			case WormSpeed_ID : {
				float num = 0;
				in >> num;
				WormSpeed = num;
				break;
			}
			case ShellMass_ID : {
				int num = 0;
				in >> num;
				ShellMass = num;
				break;
			}
			case WormMass_ID : {
				int num = 0;
				in >> num;
				WormMass = num;
				break;
			}
			case LifeBarSize_ID : {
				int num = 0;
				in >> num; 
				LifeBarSize = num;
				break;
			}
			case WormStayLeftTile_ID : {
				std::string pathfile;
				in >> pathfile >> frinfo.WormStayLeftTile.x >> frinfo.WormStayLeftTile.y >> frinfo.WormStayLeftTile.dx >> frinfo.WormStayLeftTile.dy >> frinfo.WormStayLeftTile.num_frames;
				frinfo.WormStayLeftTile.texture.loadFromFile(pathfile);
				break;
			}
			case WormStayRightTile_ID : {
				std::string pathfile;
				in >> pathfile >> frinfo.WormStayRightTile.x >> frinfo.WormStayRightTile.y >> frinfo.WormStayRightTile.dx >> frinfo.WormStayRightTile.dy >> frinfo.WormStayRightTile.num_frames;
				frinfo.WormStayRightTile.texture.loadFromFile(pathfile); 
				break;
			}
			case WormLeftTile_ID : {
				std::string pathfile;
				in >> pathfile >> frinfo.WormLeftTile.x >> frinfo.WormLeftTile.y >> frinfo.WormLeftTile.dx >> frinfo.WormLeftTile.dy >> frinfo.WormLeftTile.num_frames;
				frinfo.WormLeftTile.texture.loadFromFile(pathfile); 
				break;
			}
			case WormRightTile_ID : {
				std::string pathfile;
				in >> pathfile >> frinfo.WormRightTile.x >> frinfo.WormRightTile.y >> frinfo.WormRightTile.dx >> frinfo.WormRightTile.dy >> frinfo.WormRightTile.num_frames;
				frinfo.WormRightTile.texture.loadFromFile(pathfile);
				break;
			}
			case WormJumpLeftTile_ID : {
				std::string pathfile;
				in >> pathfile >>frinfo.WormJumpLeftTile.x >> frinfo.WormJumpLeftTile.y >> frinfo.WormJumpLeftTile.dx >> frinfo.WormJumpLeftTile.dy >> frinfo.WormJumpLeftTile.num_frames;
				frinfo.WormJumpLeftTile.texture.loadFromFile(pathfile); 
				break;
			}
			case WormJumpRightTile_ID : {
				std::string pathfile;
				in >> pathfile >> frinfo.WormJumpRightTile.x >> frinfo.WormJumpRightTile.y >> frinfo.WormJumpRightTile.dx >> frinfo.WormJumpRightTile.dy >> frinfo.WormJumpRightTile.num_frames;
				frinfo.WormJumpRightTile.texture.loadFromFile(pathfile);
				break;
			}
			case WormFallLeftTile_ID : {
				std::string pathfile;
				in >> pathfile >> frinfo.WormFallLeftTile.x >> frinfo.WormFallLeftTile.y >> frinfo.WormFallLeftTile.dx >> frinfo.WormFallLeftTile.dy >> frinfo.WormFallLeftTile.num_frames;
				frinfo.WormFallLeftTile.texture.loadFromFile(pathfile);	
				break;
			}
			case WormFallRightTile_ID : {
				std::string pathfile;
				in >> pathfile >> frinfo.WormFallRightTile.x >> frinfo.WormFallRightTile.y >> frinfo.WormFallRightTile.dx >> frinfo.WormFallRightTile.dy >> frinfo.WormFallRightTile.num_frames;
				frinfo.WormFallRightTile.texture.loadFromFile(pathfile); 
				break;
			}
			case WormDeadTile_ID : {
				std::string pathfile;
				in >> pathfile >> frinfo.WormDeadTile.x >> frinfo.WormDeadTile.y >> frinfo.WormDeadTile.dx >> frinfo.WormDeadTile.dy >> frinfo.WormDeadTile.num_frames;
				frinfo.WormDeadTile.texture.loadFromFile(pathfile);
				break;
			}
			case weaponForce_ID : {
				int num = 0;
				in >> num; 
				weaponForce = num;
				break;
			}
			case maxForce_ID : {
				int num = 0;
				in >> num;
				maxForce = num;
				break;
			}
			case reloadTime_ID : {
				int num = 0;
				in >> num; 
				reloadTime = num;
				break;
			}
			case respawnPoint1 : {
				in >> respawnPoints[0].x >> respawnPoints[0].y; 
				break;
			}
			case respawnPoint2 : {
				in >> respawnPoints[1].x >> respawnPoints[1].y;
				break;
			}
			case respawnPoint3 : {
				in >> respawnPoints[2].x >> respawnPoints[2].y;
				break;
			}
			case respawnPoint4 : {
				in >> respawnPoints[3].x >> respawnPoints[3].y; 
				break;
			}
			case waitingTime_ID : {
				in >> waitingTime;
				break;
			}
			case weaponDamage_ID : {
				in >> weaponDamage;
				break;
			}

			default : {
				std::cerr << "Unkown constant in config file" << std::endl;
			}
		}
	}
	config.close();
	return true;
}

