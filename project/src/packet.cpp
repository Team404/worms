#include "packet.hpp"

work_packet::receive(QuadTree* qt) {
		int id = 0;
		packet >> id;
		if (id == Constants::QuadTree_ID) {
			if (packet >> *qt) {
				std::cout << "Bad packet" << std::endl;
			}
			std::cout << "Print:" << std::endl << std::endl;
			qt->print();
		}
		packet.clear();
		pressed = false;
	}

work_packet::send(sf::Event& event, ServerSimulation& server, QuadTree* qt) {
	if (!pressed) {
		pressed = true;
		packet.clear();
		sf::Clock clock;
		clock.restart();
		packet << Constants::Explosion_ID;
			   << event.mousebutton.x
			   << event.mousebutton.y
			   << 16;
		sf::Packet inputPacket;
		server.explosion.QuadTree(packet, inputPacket);
		int id = 0;
		inputPacket >> id;			
		if (id == Constants::QuadTree_ID) {
			std::cout << "Good Id" << std::endl;
			inputPacket >> *qt;
		} else if {
			std::cout << "Bad id" << std::endl;
		}
		packet.clear();
	}
}
