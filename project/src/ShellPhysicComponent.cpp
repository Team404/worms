#include "ShellPhysicComponent.hpp"

void ShellPhysicComponent::update(float time) {
		falling(time);
		handleVelocity(time);
	}

void ShellPhysicComponent::getImpulse(sf::Vector2f umpulsVelocity) {
	velocity += umpulsVelocity;
}

void ShellPhysicComponent::falling(float time) {
	if (checkVertical(1)) {
		// std::cout << "falling ";
		getImpulse(Config::getInstance()->Gravity * (time / (Config::getInstance()->GameSpeed * 4) * mass));
	} else {
		velocity.y = 0;
		state->explose = 1;
	}
}

void ShellPhysicComponent::handleVelocity(float time) {
	accumulatedMove += velocity * (time / 2);
	if (abs(accumulatedMove.x) >= 1 || abs(accumulatedMove.y) >= 1) {
		move(sf::Vector2i((int)accumulatedMove.x, (int)accumulatedMove.y));
		if (abs(accumulatedMove.x) >= 1) {
			accumulatedMove.x = 0;
		}
		if (abs(accumulatedMove.y) >= 1) {
			accumulatedMove.y = 0;
		}
	}
	state->direction = atan(velocity.y / velocity.x) * 180 / 3.14;
	if (velocity.x < 0) {
		state->direction += 180;
	}
}

bool ShellPhysicComponent::checkVertical(int mode) { // -1 - down, 1 - up  true - можно двигаться, false - нельзя
	int y = (mode < 0) ? state->rect.top -1 : state->rect.top + state->rect.height;
	for (int i = state->rect.left; i < state->rect.left + state->rect.width; ++i) {
		try{
			if (qt.getNodeState(i, y) == 1) {
				return false;
			}
			for (auto& rect : collisionRects) {
				if (rect.contains(i, y)) {
					return false;
				}
			}
		} catch(RangeException& exception) {
			return !exception.getDown();
		}
	}
	return true;
}

bool ShellPhysicComponent::checkHorizontal(int mode) { // -1 - left, 1 - right  true - можно двигаться, false - нельзя
	int x = (mode < 0) ? state->rect.left -1 : state->rect.left + state->rect.width;
	for (int i = state->rect.top; i < state->rect.top + state->rect.height; ++i) {
		try{
			if (qt.getNodeState(x, i) == 1) {
				return false;
			}
			for (auto& rect : collisionRects) {
				if (rect.contains(x, i)) {
					return false;
				}
			}
		} catch(RangeException& exception) {
			return !exception.getDown();;
		}
	}
	return true;
}

int ShellPhysicComponent::move(sf::Vector2i direction) {  // [-1; 1]
	if (!state->explose) {
		if (checkHorizontal((direction.x > 0)? 1 : -1)) {
			state->rect.left += direction.x;
		} else {
			state->explose = 1;
		}

		if (checkVertical((direction.y > 0)? 1 : -1)) {
			state->rect.top += direction.y;
		} else {
			state->explose = 1;
		}
		return 1;
	}
	return 0;
}

void ShellPhysicComponent::pushCollision(sf::Rect<sf::Int16>& rect) {
	collisionRects.push_back(rect);
}

void ShellPhysicComponent::clearCollision() {
	collisionRects.clear();
}