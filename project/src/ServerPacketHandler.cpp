#include "ServerPacketHandler.hpp"

void ServerPacketHandler::handleStatus(sf::Packet& packet) {
	sf::Uint8 number = 0;
	UnitStatus status;
	packet >> number >> status;
	state.updateWormState(number, status);
}

void ServerPacketHandler::handleExplosion(sf::Packet& packet) {
	packet.clear();
}

void ServerPacketHandler::handleQT(sf::Packet& packet) {
	packet.clear();
}

void ServerPacketHandler::handleNumber(sf::Packet& packet) {
	packet.clear();
}

void ServerPacketHandler::handleShells(sf::Packet& packet) {
	packet.clear();
}

void ServerPacketHandler::handleWin(sf::Packet& packet) {
	packet.clear();
}