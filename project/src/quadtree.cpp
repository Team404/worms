#include <iostream>
#include "quadtree.hpp"


Data Data::subData(int mode) {
	int nx = x;
	int ny = y;

	if (mode == 1 || mode == 3) {
		nx = x + size / 2;
	}

	if (mode == 2 || mode == 3) {
		ny = y + size / 2;
	}

	return Data(nx, ny, size / 2, state);
}


QuadTree* QuadTree::getNode(int x, int y) {
	// std::cout << "(" << data.x << "; " << data.y << ") size = " << data.size << ", state = " << state << std::endl;
	unsigned ux = x;
	unsigned uy = y;
	if (ux >= data.x + data.size || uy >= data.y + data.size) {
		if (y - ((int)data.size) > 0) {
			throw RangeException(true);
		}
		throw RangeException(false);
	}

	if (data.size == 1) {
		return this;
	}
	separateNode();
	int mode = 0;
	if (ux >= data.x + data.size/2) {
		mode += 1;
	}

	if (uy >= data.y + data.size/2) {
		mode += 2;
	}
	// std::cout << "mode: " << mode << std::endl;


	return childs[mode]->getNode(x, y);
}

void QuadTree::setState(short st) {
	data.state = st;
	if (*childs) {
		for(int i = 0; i < 4; ++i) {
			childs[i]->data.state = data.state;
		}
	}
	updateState();
	pool();
}

void QuadTree::pool() {
	if (data.size == 1) {
		return;
	}

	if (!*childs) {
		return;
	}
	if (checkEqualState()) {
		data.state = childs[0]->data.state;
		for(int i = 0; i < 4; ++i) {
			delete childs[i];
			childs[i] = nullptr;
		}
	} else {
		for (int i = 0; i < 4; ++i) {
			childs[i]->pool();
		}
	}
}

void QuadTree::separateNode() {
	if (data.size == 1) {
		return;
	}

	if (*childs) {
		return;
	}

	for(int i = 0; i < 4; ++i) {
		childs[i] = new QuadTree(data.subData(i), this);
	}
}

void QuadTree::separate() {
	if (data.size > 1) {
		separateNode();
		for (int i = 0; i < 4; ++i) {
			childs[i]->separate();
		}
	}
}

QuadTree::~QuadTree() {
	if(*childs) {
		for(int i = 0; i < 4; ++i) {
			delete childs[i];
			childs[i] = nullptr;
		}
	}
}

void QuadTree::print() {  // Debuf function
	if (*childs) {
		for(int i = 0; i < 4; ++i) {
			childs[i]->print();
		}
	}
	std::cout << "(" << data.x << "; " << data.y << ") size = " << data.size << ", state = " << data.state << std::endl;
}

void QuadTree::updateState() {
	if (*childs) {
		if (checkEqualState()) {
			data.state = childs[0]->data.state;
		} else {
			data.state = -1;
		}
	}
	if (parent) {
		parent->updateState();
	}
}

bool QuadTree::checkEqualState() {
	if (!*childs) {
		return true;
	}
	for (int i = 0; i < 3; ++i) {
		/**
		 *  Дополнительное проверка на -1 нужна,
		 *чтобы избежать лишних ошибок, так -1 - неопредленное состояние
			 */
		if (childs[i]->data.state == -1 || (childs[i]->data.state != childs[i+1]->data.state))  {
			return false;
		}
	}
	return true;
}

void QuadTree::explosion(unsigned x, unsigned y, int r) {
	for (int i = -r; i < r; ++i) {
		for (int j = -r; j < r; ++j) {
			if (i*i + j*j <= r*r) {
				try {
					getNode(x + j, y + i)->setState(0);
				}
				catch(RangeException& exc) {

				}
			}
		}
	}
	pool();
}

short QuadTree::getNodeState(int x, int y) const {
	unsigned ux = x;
	unsigned uy = y;
	if (ux >= data.x + data.size || uy >= data.y + data.size) {
		if (y - ((int)data.size) > 0) {
			throw RangeException(true);
		}
		throw RangeException(false);
	}

	if (data.state != -1) {
		return data.state;
	}

	int mode = 0;

	if (ux >= data.x + data.size/2) {
		mode += 1;
	}

	if (uy >= data.y + data.size/2) {
		mode += 2;
	}

	return childs[mode]->getNodeState(x, y);

}

sf::Packet& operator <<(sf::Packet& packet, const Data& data) {
	return packet << data.x << data.y << data.size << data.state;
}

sf::Packet& operator <<(sf::Packet& packet, const QuadTree& qt) {
	// std::cout << qt.data.x << "; " << qt.data.y << "  size = " << qt.data.size << ", state = "  << qt.state << std::endl;
	packet << qt.data;
	if (qt.data.state == -1) {
		for (int i = 0; i < 4; ++i) {
			packet << *qt.childs[i];
		}
	}
	return packet;
}

sf::Packet& operator >>(sf::Packet& packet, Data& data) {
	packet >> data.x >> data.y >> data.size >> data.state;
	return packet;
}

sf::Packet& operator >>(sf::Packet& packet, QuadTree& qt) {
	packet >> qt.data;
	qt.parent = nullptr;
	if (qt.data.state == -1) {
		qt.separateNode();
		for (int i = 0; i < 4; ++i) {
			packet >> *qt.childs[i];
			qt.childs[i]->parent = &qt;
		}
	} else {
		for (int i = 0; i < 4; ++i) {
			delete qt.childs[i];
			qt.childs[i] = nullptr;
		}
	}
	return packet;
}