#include "ServerWorm.hpp"

void ServerWorm::getImpulse(sf::Vector2f impulse) {
	phc.getImpulse(impulse);
}

void ServerWorm::decreaseHP(float diff) {
	if (fabs(diff) > 100) {
		getState().hp = 0;
		return;
	}
	getState().hp -= fabs(diff);
	if (getState().hp < 0) {
		getState().hp = 0;
	}
}

void ServerWorm::update(float time) {
	phc.update(time);
	updateShooting(time);
	for (auto& weapon : weapons) {
		weapon->update(getState().rect.left + getState().rect.width/2,
		               getState().rect.top + getState().rect.height/2);
	}
}

ServerWeapon& ServerWorm::getCurrentWeapon() {
	return *weapons[getState().currentWeapon];
}

void ServerWorm::updateShooting(float time) {
	if (getState().shooting == Shooting::SHOOTING) {
		getCurrentWeapon().updateShooting(time);
	}
	getCurrentWeapon().updateReload(time);
}

std::unique_ptr<ServerShell> ServerWorm::shoot() {
	std::unique_ptr<ServerShell> shell = std::move(getCurrentWeapon().shoot(sf::Vector2f(cos(getState().direction * PI / 180),
	                                                                           	         sin(getState().direction * PI / 180) * -1)));
	if (!shell) {
		getState().shooting = Shooting::NOT_SHOOT;
	}
	return shell;
}
