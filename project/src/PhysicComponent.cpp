#include "PhysicComponent.hpp"

#include "ConstConfig.hpp"

void PhysicComponent::update(float time) {
		// std::cout << *us << std::endl;
		// std::cout << "update time is " << time << std::endl;
	switch (us->onGround) {
		case ON_GROUND : {
			moveVelocity.x = (us->moveRight - us->moveLeft) * speed;
			break;
		}

		case READY_TO_JUMP : {
			if (!checkVertical(1) || moveDown < 5) {
				velocity.y = Config::getInstance()->Space.y;
			} else {
				us->onGround = ON_GROUND;
			}
			break;
		}

		case JUMP : {
			moveVelocity.x = (us->moveRight - us->moveLeft) * speed;
		}

		default : {
		}
	}
	friction(time);
	falling(time);
	handleVelocity(time);

}

bool PhysicComponent::checkVertical(int mode) { // -1 - down, 1 - up  true - можно двигаться, false - нельзя
	for (int i = us->rect.left; i < us->rect.left + us->rect.width; ++i) {
		try{
			if (qt.getNodeState(i, ((mode < 0) ? us->rect.top -1 : us->rect.top + us->rect.height)) == 1) {
				return false;
			}
		} catch(RangeException& exception) {
			if (exception.getDown()) {
				us->hp = 0;
				return false;
			}
			return true;
		}
	}
	return true;
}

bool PhysicComponent::checkHorizontal(int mode, int& findY) {  // mode: 1 - right, -1 - left
	int x = (mode == 1) ? us->rect.left + us->rect.width : us->rect.left - 1;
	int miny = us->rect.top;
	int maxy = us->rect.top + us->rect.height;
	try {
		for (int i = maxy; i >= us->rect.top; --i) {
			if (qt.getNodeState(x, i) == 0) {
				maxy = i;
				break;
			}
		}

		if (us->rect.top + us->rect.height - maxy > us->rect.height/2) {
			return false;
		}

		for (int i = maxy; i >= maxy - us->rect.height; --i) {
			miny = i;
			if (qt.getNodeState(x, i) != 0) {
				break;
			}
		}
	} catch(RangeException& exception) {
		return false;
	}
	if (maxy - miny >= us->rect.height) {
		findY = maxy;
		return true;
	}
	return false;
}

int PhysicComponent::move(sf::Vector2i direction) {  // [-1; 1]
	int returnValue = 0;
	if (direction.y != 0) {
		if (checkVertical((direction.y > 0)? 1 : -1)) {
			us->rect.top += direction.y;
			returnValue += 1;
		}
	}

	if (direction.x != 0) {
		int findY = us->rect.top + us->rect.height;
		if (checkHorizontal(((direction.x > 0)? 1 : -1), findY)) {
			us->rect.left += direction.x;
			us->rect.top = findY - us->rect.height;			
			returnValue += 2;
		} else {
			velocity.x = 0;
		}
	}
	return returnValue;
}

void PhysicComponent::getImpulse(sf::Vector2f umpulsVelocity) {
	velocity += umpulsVelocity;
}

void PhysicComponent::handleVelocity(float time) {
	accumulatedMove += (velocity + moveVelocity) * (time / 2);
	if (abs(accumulatedMove.x) >= 1 || abs(accumulatedMove.y) >= 1) {
		if (abs(accumulatedMove.x) >= 2 || abs(accumulatedMove.y) >= 2) {
			std::cout << accumulatedMove.x << "; " << accumulatedMove.y  << "Time: " << time * Config::getInstance()->GameSpeed << std::endl;
		}
		move(sf::Vector2i((int)accumulatedMove.x, (int)accumulatedMove.y));
		if (accumulatedMove.y > 0) {
			moveDown += accumulatedMove.y;
		}

		if (!checkVertical(-1)) {
			if (velocity.y < 0) {
				velocity.y *= -0.8;
			}
		}

		if (us->onGround == READY_TO_JUMP) {
			us->onGround = JUMP;
		}
		if (abs(accumulatedMove.x) >= 1) {
			accumulatedMove.x = 0;
		}
		if (abs(accumulatedMove.y) >= 1) {
			accumulatedMove.y = 0;
		}
	}
}

void PhysicComponent::falling(float time) {
	if (checkVertical(1)) {
		getImpulse(Config::getInstance()->Gravity * (time / (Config::getInstance()->GameSpeed * 4) * mass));
		if (us->onGround != JUMP && us->onGround != READY_TO_JUMP ) {
			if (moveDown > 5) {
				us->onGround = FALLING;
			} else {
				us->onGround = CLIMBING;
			}
		}
	}
	else {
		if (us->onGround != ON_GROUND && us->onGround != READY_TO_JUMP) {
			us->onGround = ON_GROUND;
			velocity.y = 0;
			/*if (velocity.x > 0.1 || velocity.x < -0.1) {
				getImpulse(0.2f * ((velocity.x > 0) ? -1 : 1 ) * sf::Vector2f(Config::getInstance()->Gravity.y, 0));
			} else {
				velocity.x = 0;
			}*/
			moveVelocity.y = 0;
		}
		moveDown = 0;
	}
}
void PhysicComponent::friction(float time) {
	if (us->onGround != FALLING && us->onGround != JUMP) {
		
		switch (FrictionForceMode) {
			case 0 : {
				FrictionForceMode = (velocity.x > 0) ? 1 : -1;
				break;
			}

			case 1 : {
				getImpulse( -0.2f * sf::Vector2f(Config::getInstance()->Gravity.y, 0));
				if (velocity.x  < 0) {
					velocity.x = 0;
					FrictionForceMode = 0;
				}
				break;
			}

			case -1 : {
				getImpulse( Config::getInstance()->FrictionCoefficient * sf::Vector2f(Config::getInstance()->Gravity.y, 0) * (time / (Config::getInstance()->GameSpeed * 4) * mass));
				if (velocity.x  > 0) {
					velocity.x = 0;
					FrictionForceMode = 0;
				}
				break;
			}
		}
	}
}
