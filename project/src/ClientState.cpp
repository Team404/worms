#include "ClientState.hpp"

void ClientState::update(float time) {
	for (auto& worm : worms) {
		//if (worm->isAlive()) {
			if (win) {
				worm->getState().moveLeft = 0;
				worm->getState().moveRight = 0;
			}
			worm->update(time);
		//}
	}

	for (auto& shell : shells) {
		shell->update();
	}
}

UnitStatus& ClientState::getPlayerState() {
	return worms[wormNumber]->getState();
}

QuadTree& ClientState::getQT() {
	return qt;
}

std::vector<std::unique_ptr<Worm>>& ClientState::getWorms() {
	return worms;
}

std::list<std::unique_ptr<ClientShell>>& ClientState::getShells() {
	return shells;
}

void ClientState::explosion(int x, int y, int r) {
	qt.explosion(x, y, r);
}

void ClientState::setWormNumber(int number) {
	wormNumber = number;
	std::cout  << "wormNumber is " << wormNumber << std::endl;
}

sf::Int8 ClientState::getWormNumber() {
	return wormNumber;
}

void ClientState::updateWormState(std::vector<std::unique_ptr<UnitStatus>>& states) {
	for (unsigned i = 0; i < states.size(); ++i) {
		if (i == worms.size()) {
			worms.push_back(std::move(static_unique_pointer_cast<Worm>(std::move((factory.create())))));
			worms.at(i)->getState() = *states.at(i);
		} else if (i == wormNumber) {
			worms.at(i)->getState().rect = states.at(i)->rect;
			worms.at(i)->getState().onGround = states.at(i)->onGround;
			worms.at(i)->getState().hp = states.at(i)->hp;
		} else {
			worms.at(i)->getState() = *states.at(i);
		}
	}
}

void ClientState::setWin(int win) {
	this->win = win;
}

int ClientState::getWin() {
	return win;
}