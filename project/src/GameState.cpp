#include "GameState.hpp"
#include "ConstConfig.hpp"

void GameState::update(float time) {
	shells.remove_if([](std::unique_ptr<ServerShell>& shell){ return !shell->isAlive(); });

	// std::cout << "shells size = " << shells.size() << std::endl;
	int i = 0;
	for (auto& worm : worms) {
		if (worm->isAlive()) {
			++i;
		}
		
		worm->update(time);
		if (worm->getState().shooting == Shooting::SHOOT) {
			std::unique_ptr<ServerShell> shell = std::move(worm->shoot());
			if (shell) {
				shells.push_back(std::move(shell));
			}
			else {
				worm->getState().shooting = Shooting::NOT_SHOOT;
			}
		}
	}
	countAlive = i;

	for (auto& shell : shells) {
		shell->update(time);
		if (!shell->isAlive()) {
			qt.explosion(shell->getState().rect.left, shell->getState().rect.top, shell->getState().radius);
			explosions.emplace_back(shell->getState().rect.left, shell->getState().rect.top, shell->getState().radius);
		}
		shell->clearCollision();
		for (auto& worm : worms) {
			if (worm->isAlive()) {
				shell->pushCollision(worm->getState().rect);
			}
		}
	}
}

unsigned GameState::getCountAlive() {
	return countAlive;
}

unsigned GameState::getPlayerCount() {
	return worms.size();
}

void GameState::handleWormExplosion(ServerWorm& worm, Explosion& expl) {
	for (int i = -expl.r; i < expl.r; ++i) {
		for (int j = -expl.r; j < expl.r; ++j) {
			if (i*i + j*j <= expl.r*expl.r) {
				if (worm.getState().rect.contains(expl.x + j, expl.y + i)) {
					float xForce = (worm.getState().rect.left + worm.getState().rect.width / 2 - expl.x);
					float yForce = (worm.getState().rect.top + worm.getState().rect.height / 2 - expl.y);
					float range = xForce * xForce + yForce * yForce;
					worm.getImpulse(15.0f * sf::Vector2f(xForce / range , yForce / range));
					if (worm.isAlive()) {
						worm.decreaseHP(Config::getInstance()->weaponDamage / (range));
					}
					return;
				}
			}
		}
	}
}

void GameState::handleExplosions() {
	for (auto& explosion : explosions) {
		for (auto& worm : worms) {
			handleWormExplosion(*worm, explosion);
		}
	}
}

std::vector<Explosion>& GameState::getExplosions() {
	return explosions;
}

std::list<std::unique_ptr<ServerShell>>& GameState::getShells() {
	return shells;
}

void GameState::clearExplosions() {
	explosions.clear();
}

void GameState::makeExplosion(unsigned x, unsigned y, unsigned r) {
	qt.explosion(x, y, r);
}

void GameState::updateWormState(sf::Uint8 number, UnitStatus& state) {
	if (worms.at(number)->isAlive()) {
		if (countAlive > 1) {
			worms.at(number)->getState().moveLeft = state.moveLeft; 
			worms.at(number)->getState().moveRight = state.moveRight;
			worms.at(number)->getState().shooting = state.shooting;
		} else {
			worms.at(number)->getState().moveLeft = 0; 
			worms.at(number)->getState().moveRight = 0;
			worms.at(number)->getState().shooting = NOT_SHOOT;
		}
		worms.at(number)->getState().rect.height = state.rect.height;
		worms.at(number)->getState().rect.width = state.rect.width;
		worms.at(number)->getState().currentWeapon = state.currentWeapon;
		if ((state.onGround == JUMP || state.onGround == READY_TO_JUMP) && worms.at(number)->getState().onGround != JUMP) {
			worms.at(number)->getState().onGround = state.onGround;
		}
		worms.at(number)->getState().direction = state.direction;
	}
}


QuadTree& GameState::getQT() {
	return qt; 
}

std::vector<std::unique_ptr<ServerWorm>>& GameState::getWorms() {
	return worms;
}