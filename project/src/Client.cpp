#include <SFML/Network.hpp>
#include <iostream>
#include <memory>

#include "Unit.hpp"
#include "Animations.hpp"
#include "Button.hpp"
#include "ListenerButton.hpp"
#include "TextBar.hpp"
#include "Parser.hpp"
#include "Client.hpp"
#include "Game.hpp"

Client::Client() {
	font.loadFromFile("fonts/minecraft.ttf");
	connected = sf::Text("CONNECTED!", font, 40);
	connected.setStyle(sf::Text::Bold);
	failed = sf::Text("Waiting is too long. Disconnecting", font, 40);
	failed.setStyle(sf::Text::Bold);
	waitQT = sf::Text("CREATING THE MAP", font, 30);
	waitPL = sf::Text("WAITING FOR PLAYERS", font, 30);
	waitST = sf::Text("WAITING FOR SERVER", font, 30);
}

bool Client::receivePacket() {
	sf::Packet packet;
 	bool ret = false;
 	while (socket.receive(packet) == sf::Socket::Done) {
 		inputPackets.emplace_back(std::make_unique<sf::Packet>(packet));
		// std::cout << "RESEIVE" << std::endl;
 		ret = true;
 	}
 	if (ret) {
		ph->handleVector(inputPackets);
		clearPackets();
 	}
 	return ret;
}

void Client::handleEvent(sf::Event event, sf::RenderWindow& win) {
	if (event.type == sf::Event::MouseButtonPressed) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			if (state->getPlayerState().shooting != Shooting::SHOOTING) {
				state->getPlayerState().shooting = Shooting::SHOOTING;
			}
		}
		if (event.mouseButton.button == sf::Mouse::Right) {
			state->getQT().print();
		}
	}
	if (event.type == sf::Event::MouseButtonReleased) {
		if (state->getPlayerState().shooting == Shooting::SHOOTING) { 
			state->getPlayerState().shooting = Shooting::SHOOT;
		} else {
			state->getPlayerState().shooting = Shooting::NOT_SHOOT;
			std::cout << "NOT_SHOOT" << std::endl;
		}
	}

	if (event.type == sf::Event::KeyPressed) {
		if (event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::A) {
			state->getPlayerState().moveLeft = 1;
		}
		if (event.key.code == sf::Keyboard::Right || event.key.code == sf::Keyboard::D) {
			state->getPlayerState().moveRight = 1;
		}
		if (event.key.code == sf::Keyboard::Space) {
			state->getPlayerState().onGround = READY_TO_JUMP;
		}
	}
	if (event.type == sf::Event::KeyReleased) {
		if (event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::A) {
			state->getPlayerState().moveLeft = 0;
		}
		if (event.key.code == sf::Keyboard::Right || event.key.code == sf::Keyboard::D) {
			state->getPlayerState().moveRight = 0;
		}
		if (event.key.code == sf::Keyboard::Space) {
			state->getPlayerState().onGround = JUMP;
		}	
	}

	sf::Vector2i mousePos = static_cast<sf::Vector2i>(win.mapPixelToCoords(sf::Mouse::getPosition(win)));
	float dx = mousePos.x - (state->getPlayerState().rect.left + (state->getPlayerState().rect.width / 2));
	float dy = mousePos.y - (state->getPlayerState().rect.top + (state->getPlayerState().rect.height / 2));

	state->getPlayerState().direction = mouseAngle(dx, dy);
}
