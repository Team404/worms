#include "UnitStatus.hpp"

std::ostream& operator <<(std::ostream& os, UnitStatus& us) {
	os << "Rect : " << us.rect.left << "; " << us.rect.top << std::endl << us.direction <<  " " <<  (int)us.onGround <<  " " <<  (int)us.moveRight <<  " " <<  (int)us.moveLeft <<  " " <<  (int)us.shooting;
	return os;
}

sf::Packet& operator <<(sf::Packet& packet, const sf::Rect<sf::Int16>& rect) {
	packet << rect.left << rect.top << rect.width << rect.height;
	return packet;
}

sf::Packet& operator <<(sf::Packet& packet, const UnitStatus& us) {
	packet << us.rect << us.direction << static_cast<sf::Int8>(us.onGround) << us.moveRight << us.moveLeft << static_cast<sf::Int8>(us.shooting) << us.currentWeapon << us.hp;
	return packet;
}

sf::Packet& operator >>(sf::Packet& packet, sf::Rect<sf::Int16>& rect) {
	packet >> rect.left >> rect.top >> rect.width >> rect.height;
	return packet;
}

sf::Packet& operator >>(sf::Packet& packet, UnitStatus& us) {
	sf::Int8 shooting = 0;
	sf::Int8 onGround = 0;
	packet >> us.rect >> us.direction >> onGround >> us.moveRight >> us.moveLeft >> shooting >> us.currentWeapon >> us.hp;
	us.onGround = static_cast<OnGround>(onGround);
	us.shooting = static_cast<Shooting>(shooting);
	return packet;
}