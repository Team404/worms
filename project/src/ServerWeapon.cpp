#include "ServerWeapon.hpp"

void ServerWeapon::update(int x, int y) {
	pos.x = x;
	pos.y = y;
}

void ServerWeapon::updateReload(float time) { 
	reload -= fabs(time);
	if (reload < 0) {
		reload = 0;
	}
}

void ServerWeapon::updateShooting(float time) {
	if (reload == 0) {
		if (maxForce == 1) {
			shootingForce = 1;
		} else {
 		shootingForce += time;
 		if (shootingForce > maxForce) {
 			shootingForce = maxForce;
 		}
 	}
}
}

std::unique_ptr<ServerShell> ServerWeapon::shoot(sf::Vector2f impulse) {
	if (reload == 0 && shootingForce > 0) {
		std::unique_ptr<ServerShell> shell = std::move(factory->create(pos, shootingForce * weaponForce * impulse));
		shootingForce = 0;
		reload = reloadTime;
		return shell;
	}
	return std::unique_ptr<ServerShell>();
}