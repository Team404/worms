Установка sfml:
1. Распаковать архив на месте. 
2. В новой папке SFML-2.5.0 пишем `cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=TRUE -G "Unix Makefiles"`. После этого может жаловаться, что не установлены какие-то либы. Для этого пишем sudo apt-get install lib<название либы>-dev
3. После успешного окончания пишем make.
4. При сборке программы может ругаться на ld linker error “cpu model hidden symbol”. Переходим по https://stackoverflow.com/questions/38727800/ld-linker-error-cpu-model-hidden-symbol и читаем, что куда нужно вставить. После этого goto 2;
5. Перед запуском программы, чтобы подключить либы, нужно написать `export LD_LIBRARY_PATH=SFML-2.5.0/lib`.
6. Поздравляю. Вы установили библиотеку.

